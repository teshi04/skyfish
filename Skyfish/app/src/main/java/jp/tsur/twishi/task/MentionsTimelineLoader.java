package jp.tsur.twishi.task;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import jp.tsur.twishi.TwitterManager;
import twitter4j.Paging;
import twitter4j.ResponseList;
import twitter4j.Status;


/**
 * メンションタイムラインを取得
 */
public class MentionsTimelineLoader extends AsyncTaskLoader<ResponseList<Status>> {

    private long mMaxId;

    public MentionsTimelineLoader(Context context, long maxId) {
        super(context);
        mMaxId = maxId;
    }

    @Override
    public ResponseList<Status> loadInBackground() {
        try {
            Paging paging = new Paging();
            if (mMaxId > 0) {
                paging.setMaxId(mMaxId - 1);
                paging.setCount(20);//BasicSettings.getPageCount());
            }
            return TwitterManager.getTwitter().getMentionsTimeline(paging);
        } catch (OutOfMemoryError e) {
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}