package jp.tsur.twishi.listener;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.HeaderViewListAdapter;

import jp.tsur.twishi.adapter.StatusAdapter;
import jp.tsur.twishi.model.BasicSettings;
import jp.tsur.twishi.model.Row;
import jp.tsur.twishi.ui.status.TalkFragment;
import jp.tsur.twishi.util.ActionUtil;
import twitter4j.Status;

public class StatusLongClickListener implements AdapterView.OnItemLongClickListener {

    private FragmentActivity mActivity;
    private int mHeaderCount;

    public StatusLongClickListener(Activity activity) {
        mActivity = (FragmentActivity) activity;
    }

    public StatusLongClickListener(Activity activity, int headerCount) {
        mActivity = (FragmentActivity) activity;
        mHeaderCount = headerCount;
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
        HeaderViewListAdapter headerViewListAdapter = (HeaderViewListAdapter) adapterView.getAdapter();
        StatusAdapter adapter = (StatusAdapter) headerViewListAdapter.getWrappedAdapter();
        if (mHeaderCount > 0) {
            position -= mHeaderCount;
        }

        Row row = adapter.getItem(position);
        if (row == null) {
            return false;
        }
        if (row.isDirectMessage()) {
            return false;
        }

        Status status = row.getStatus();
        final Status retweet = status.getRetweetedStatus();
        final Status source = retweet != null ? retweet : status;

        Bundle args = new Bundle();
        String action = BasicSettings.getLongTapAction();
        switch (action) {
            case "quote":
                ActionUtil.doQuote(source, mActivity);
                break;
            case "talk":
                if (source.getInReplyToStatusId() > 0) {
                    TalkFragment dialog = new TalkFragment();
                    args.putSerializable("status", source);
                    dialog.setArguments(args);
                    dialog.show(mActivity.getSupportFragmentManager(), "dialog");
                } else {
                    return false;
                }
                break;
            case "show_around":
//            AroundFragment aroundFragment = new AroundFragment();
//            Bundle aroundArgs = new Bundle();
//            aroundArgs.putSerializable("status", source);
//            aroundFragment.setArguments(aroundArgs);
//            aroundFragment.show(mActivity.getSupportFragmentManager(), "dialog");
                break;
            case "share_url":
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, "https://twitter.com/" + status.getUser().getScreenName()
                        + "/status/" + String.valueOf(status.getId()));
                mActivity.startActivity(intent);
                break;
            case "reply_all":
                ActionUtil.doReplyAll(source, mActivity);
                break;
            default:
                return false;
        }
        return true;
    }
}
