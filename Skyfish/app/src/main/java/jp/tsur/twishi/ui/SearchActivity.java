package jp.tsur.twishi.ui;

import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import jp.tsur.twishi.R;
import jp.tsur.twishi.TwitterManager;
import jp.tsur.twishi.adapter.StatusAdapter;
import jp.tsur.twishi.event.AlertDialogEvent;
import jp.tsur.twishi.event.StatusActionEvent;
import jp.tsur.twishi.event.StreamingDestroyStatusEvent;
import jp.tsur.twishi.listener.StatusClickListener;
import jp.tsur.twishi.listener.StatusLongClickListener;
import jp.tsur.twishi.model.Row;
import jp.tsur.twishi.util.KeyboardUtil;
import jp.tsur.twishi.util.MessageUtil;
import jp.tsur.twishi.widget.ClearEditText;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.SavedSearch;

public class SearchActivity extends ActionBarActivity implements LoaderManager.LoaderCallbacks<QueryResult> {

    public static final String EXTRA_QUERY = "query";

    @InjectView(R.id.search_word_text)
    ClearEditText mSearchWordText;
    @InjectView(R.id.search_button)
    Button mSearchButton;
    @InjectView(R.id.search_list)
    ListView mSearchList;
    @InjectView(R.id.progress_bar)
    ProgressBar mProgressBar;

    private StatusAdapter mAdapter;
    private Query mNextQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.inject(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        // Status(ツイート)をViewに描写するアダプター
        mAdapter = new StatusAdapter(this, R.layout.list_item_status);
        mSearchList.setAdapter(mAdapter);

        mSearchList.setOnItemClickListener(new StatusClickListener(this));
        mSearchList.setOnItemLongClickListener(new StatusLongClickListener(this));

        mSearchWordText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //EnterKeyが押されたかを判定
                if (event.getAction() == KeyEvent.ACTION_DOWN
                        && keyCode == KeyEvent.KEYCODE_ENTER) {
                    search();
                    return true;
                }
                return false;
            }
        });

        Intent intent = getIntent();
        String query = intent.getStringExtra(EXTRA_QUERY);
        if (query != null) {
            mSearchWordText.setText(query);
            mSearchButton.performClick();
        } else {
            KeyboardUtil.showKeyboard(mSearchWordText);
        }

        mSearchList.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                // 最後までスクロールされたかどうかの判定
                if (totalItemCount == firstVisibleItem + visibleItemCount) {
                    additionalReading();
                }
            }
        });
    }

    @OnClick(R.id.search_button)
    void onClickSearchButton() {
        search();
    }

    @OnClick(R.id.tweet_button)
    void onClickTweetButton() {
        if (mSearchWordText.getText() == null) return;
        DialogFragment dialogFragment =
                UpdateStatusFragment.newInstance(null, mSearchWordText.getText().toString(), 0, 0);
        dialogFragment.show(getFragmentManager(), "dialog");
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @SuppressWarnings("UnusedDeclaration")
    public void onEventMainThread(AlertDialogEvent event) {
        event.getDialogFragment().show(getSupportFragmentManager(), "dialog");
    }

    @SuppressWarnings("UnusedDeclaration")
    public void onEventMainThread(StatusActionEvent event) {
        mAdapter.notifyDataSetChanged();
    }

    @SuppressWarnings("UnusedDeclaration")
    public void onEventMainThread(StreamingDestroyStatusEvent event) {
        mAdapter.removeStatus(event.getStatusId());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.save_search:
                if (mSearchWordText.getText() != null) {
                    new CreateSavedSearchTask().execute(mSearchWordText.getText().toString());
                }
                break;
        }
        return true;
    }

    private void additionalReading() {
        if (mNextQuery != null) {
            mProgressBar.setVisibility(View.VISIBLE);

            Bundle args = new Bundle(1);
            args.putSerializable("query", mNextQuery);
            getSupportLoaderManager().restartLoader(0, args, this);
            mNextQuery = null;
        }
    }

    private void search() {
        KeyboardUtil.hideKeyboard(mSearchWordText);
        if (mSearchWordText.getText() == null) return;
        mAdapter.clear();
        mSearchList.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.VISIBLE);
        mNextQuery = null;

        Query query = new Query(mSearchWordText.getText().toString().concat(" exclude:retweets"));

        Bundle args = new Bundle(1);
        args.putSerializable("query", query);
        getSupportLoaderManager().restartLoader(0, args, this).forceLoad();
    }

    @Override
    public Loader<QueryResult> onCreateLoader(int id, Bundle args) {
        Query query = (Query) args.getSerializable("query");
        return new SearchLoader(this, query);
    }

    @Override
    public void onLoadFinished(Loader<QueryResult> loader, QueryResult queryResult) {
        if (queryResult == null) {
            MessageUtil.showToast(R.string.toast_load_data_failure);
            return;
        }
        if (queryResult.hasNext()) {
            mNextQuery = queryResult.nextQuery();
        }
        int count = mAdapter.getCount();
        List<twitter4j.Status> statuses = queryResult.getTweets();
        for (twitter4j.Status status : statuses) {
            mAdapter.add(Row.newStatus(status));
        }

        mSearchList.setVisibility(View.VISIBLE);
        if (count == 0) {
            mSearchList.setSelection(0);
        }
        mProgressBar.setVisibility(View.GONE);

        // インテント経由で検索時にうまく閉じてくれないので入れている
        InputMethodManager inputMethodManager =
                (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(mSearchWordText.getWindowToken(), 0);
    }

    @Override
    public void onLoaderReset(android.support.v4.content.Loader<QueryResult> loader) {

    }

    public static class SearchLoader extends AsyncTaskLoader<QueryResult> {

        private Query mQuery;

        public SearchLoader(Context context, Query query) {
            super(context);
            mQuery = query;
        }

        @Override
        public QueryResult loadInBackground() {
            try {
                return TwitterManager.getTwitter().search(mQuery);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    private class CreateSavedSearchTask extends AsyncTask<String, Void, SavedSearch> {
        @Override
        protected SavedSearch doInBackground(String... params) {
            String query = params[0];
            try {
                return TwitterManager.getTwitter().createSavedSearch(query);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(SavedSearch savedSearch) {
            if (savedSearch == null) {
                return;
            }
            MessageUtil.showToast(getString(R.string.toast_save_success));
        }
    }
}
