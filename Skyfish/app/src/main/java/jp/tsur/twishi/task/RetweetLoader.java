package jp.tsur.twishi.task;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import jp.tsur.twishi.TwitterManager;
import twitter4j.ResponseList;
import twitter4j.Status;


/**
 * リツートを取得
 */
public class RetweetLoader extends AsyncTaskLoader<ResponseList<Status>> {

    private long mStatusId;

    public RetweetLoader(Context context, long statusId) {
        super(context);
        mStatusId = statusId;
    }

    @Override
    public ResponseList<Status> loadInBackground() {
        try {
            return TwitterManager.getTwitter().getRetweets(mStatusId);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}


