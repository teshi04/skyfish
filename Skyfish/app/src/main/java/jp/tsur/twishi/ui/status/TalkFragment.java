package jp.tsur.twishi.ui.status;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.widget.ListView;

import de.greenrobot.event.EventBus;
import jp.tsur.twishi.R;
import jp.tsur.twishi.adapter.StatusAdapter;
import jp.tsur.twishi.event.StatusActionEvent;
import jp.tsur.twishi.event.StreamingDestroyStatusEvent;
import jp.tsur.twishi.listener.StatusClickListener;
import jp.tsur.twishi.listener.StatusLongClickListener;
import jp.tsur.twishi.model.Row;
import jp.tsur.twishi.task.StatusLoader;
import twitter4j.Status;

/**
 * 会話を表示
 */
public class TalkFragment extends DialogFragment
        implements LoaderManager.LoaderCallbacks<Status> {

    private StatusAdapter mAdapter;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        FragmentActivity activity = getActivity();
        Dialog dialog = new Dialog(activity, R.style.NoTitleDialog);
        ListView listView = new ListView(activity);
        dialog.setContentView(listView);

        mAdapter = new StatusAdapter(activity, R.layout.list_item_status);
        listView.setAdapter(mAdapter);

        listView.setOnItemClickListener(new StatusClickListener(activity));
        listView.setOnItemLongClickListener(new StatusLongClickListener(activity));

        return dialog;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Status status = (Status) getArguments().getSerializable("status");
        assert status != null;
        mAdapter.add(Row.newStatus(status));
        mAdapter.notifyDataSetChanged();
        Bundle args = new Bundle();
        args.putLong("status_id", status.getInReplyToStatusId());
        getLoaderManager().initLoader(0, args, this).forceLoad();
    }

    @Override
    public Loader<Status> onCreateLoader(int id, Bundle args) {
        return new StatusLoader(getActivity(), args.getLong("status_id"));
    }

    @Override
    public void onLoadFinished(Loader<Status> loader, Status status) {
        if (status != null) {
            mAdapter.add(Row.newStatus(status));
            mAdapter.notifyDataSetChanged();
            Long inReplyToStatusId = status.getInReplyToStatusId();
            if (inReplyToStatusId > 0) {
                Bundle args = new Bundle();
                args.putLong("status_id", inReplyToStatusId);
                getLoaderManager().restartLoader(0, args, this).forceLoad();
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Status> loader) {

    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @SuppressWarnings("UnusedDeclaration")
    public void onEventMainThread(StatusActionEvent event) {
        mAdapter.notifyDataSetChanged();
    }

    @SuppressWarnings("UnusedDeclaration")
    public void onEventMainThread(StreamingDestroyStatusEvent event) {
        mAdapter.removeStatus(event.getStatusId());
    }
}
