package jp.tsur.twishi.task;

import android.os.AsyncTask;

import jp.tsur.twishi.FavRetweetManager;
import jp.tsur.twishi.R;
import jp.tsur.twishi.TwitterManager;
import jp.tsur.twishi.util.MessageUtil;
import jp.tsur.twishi.util.TwitterUtil;
import twitter4j.TwitterException;

//TODO:

/**
 * リツイートを解除
 */
public class DestroyRetweetTask extends AsyncTask<Void, Void, TwitterException> {

    private long mRetweetedStatusId;
    private long mStatusId;

    public DestroyRetweetTask(long retweetedStatusId, long statusId) {
        mRetweetedStatusId = retweetedStatusId;
        mStatusId = statusId;
        if (mRetweetedStatusId > 0) {
            FavRetweetManager.setRtId(mRetweetedStatusId, null);
//            EventBus.getDefault().post(new StatusActionEvent());
        }
    }

    @Override
    protected TwitterException doInBackground(Void... params) {
        try {
            TwitterManager.getTwitter().destroyStatus(mStatusId);
            return null;
        } catch (TwitterException e) {
            e.printStackTrace();
            return e;
        }
    }

    @Override
    protected void onPostExecute(TwitterException e) {
        if (e == null) {
            MessageUtil.showToast(R.string.toast_destroy_retweet_success);
        } else if (e.getErrorCode() == TwitterUtil.ERROR_CODE_UNFAVORITE_ALREADY) {
            MessageUtil.showToast(R.string.toast_destroy_retweet_already);
        } else {
            if (mRetweetedStatusId > 0) {
                FavRetweetManager.setRtId(mRetweetedStatusId, mStatusId);
//                EventBus.getDefault().post(new StatusActionEvent());
            }
            MessageUtil.showToast(R.string.toast_destroy_retweet_failure);
        }
    }
}