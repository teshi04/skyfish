package jp.tsur.twishi.task;

import android.os.AsyncTask;

import jp.tsur.twishi.R;
import jp.tsur.twishi.TwitterManager;
import jp.tsur.twishi.util.MessageUtil;
import twitter4j.DirectMessage;

/**
 * ダイレクトメッセージを削除
 */
public class DestroyDirectMessageTask extends AsyncTask<Long, Void, DirectMessage> {


    @Override
    protected DirectMessage doInBackground(Long... params) {
        try {
            final Long destroyDirectId = params[0];
            return TwitterManager.getTwitter().destroyDirectMessage(destroyDirectId);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(DirectMessage directMessage) {
        if (directMessage != null) {
            MessageUtil.showToast(R.string.toast_destroy_direct_message_success);
            // EventBus.getDefault().post(new StreamingDestroyMessageEvent(directMessage.getId()));
        } else {
            MessageUtil.showToast(R.string.toast_destroy_direct_message_failure);
        }
    }
}
