package jp.tsur.twishi.task;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import jp.tsur.twishi.TwitterManager;
import jp.tsur.twishi.model.MyDirectMessage;
import twitter4j.DirectMessage;
import twitter4j.Paging;
import twitter4j.ResponseList;
import twitter4j.Twitter;


/**
 * ダイレクトメッセージの読み込み
 */
public class DirectMessageTimelineLoader extends AsyncTaskLoader<MyDirectMessage> {

    private long mDirectMessagesMaxId;
    private long mSentDirectMessagesMaxId;

    public DirectMessageTimelineLoader(Context context, long directMessagesMaxId, long sentDirectMessagesMaxId) {
        super(context);
        mDirectMessagesMaxId = directMessagesMaxId;
        mSentDirectMessagesMaxId = sentDirectMessagesMaxId;
    }

    @Override
    public MyDirectMessage loadInBackground() {
        try {

            // 受信したDM
            Paging directMessagesPaging = new Paging();
            if (mDirectMessagesMaxId > 0) {
                directMessagesPaging.setMaxId(mDirectMessagesMaxId - 1);
            }
            directMessagesPaging.setCount(10);
            Twitter twitter = TwitterManager.getTwitter();
            ResponseList<DirectMessage> directMessages = twitter.getDirectMessages(directMessagesPaging);

            // 送信したDM
            Paging sentDirectMessagesPaging = new Paging();
            if (mSentDirectMessagesMaxId > 0) {
                sentDirectMessagesPaging.setMaxId(mSentDirectMessagesMaxId - 1);
            }
            sentDirectMessagesPaging.setCount(10);
            ResponseList<DirectMessage> sentDirectMessages = twitter.getSentDirectMessages(sentDirectMessagesPaging);

            return new MyDirectMessage(directMessages, sentDirectMessages);
        } catch (OutOfMemoryError e) {
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}