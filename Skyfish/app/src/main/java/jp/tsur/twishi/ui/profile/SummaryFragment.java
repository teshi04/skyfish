package jp.tsur.twishi.ui.profile;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import jp.tsur.twishi.AccessTokenManager;
import jp.tsur.twishi.R;
import jp.tsur.twishi.task.DestroyBlockTask;
import jp.tsur.twishi.task.DestroyFriendshipTask;
import jp.tsur.twishi.task.FollowTask;
import jp.tsur.twishi.ui.ScaleImageActivity;
import jp.tsur.twishi.util.ImageUtil;
import jp.tsur.twishi.util.MessageUtil;
import jp.tsur.twishi.widget.FontelloTextView;
import twitter4j.Relationship;
import twitter4j.User;

/**
 * プロフィール上部の左面
 */
public class SummaryFragment extends Fragment {

    @InjectView(R.id.icon_image)
    ImageView mIconImage;
    @InjectView(R.id.name_label)
    TextView mNameLabel;
    @InjectView(R.id.lock_label)
    FontelloTextView mLockLabel;
    @InjectView(R.id.screen_name_label)
    TextView mScreenNameLabel;
    @InjectView(R.id.followed_by_label)
    TextView mFollowedByLabel;
    @InjectView(R.id.follow_botton)
    Button mFollowButton;
    @InjectView(R.id.list_button)
    FontelloTextView mListButton;

    private User mUser;
    private boolean mFollowFlg;
    private boolean mBlocking;
    private boolean mRuntimeFlg;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_profile_summary, container, false);

        mUser = (User) getArguments().getSerializable("user");
        final Relationship relationship = (Relationship) getArguments().getSerializable("relationship");
        ButterKnife.inject(this, v);

        assert relationship != null;
        mFollowFlg = relationship.isSourceFollowingTarget();
        mBlocking = relationship.isSourceBlockingTarget();

        assert mUser != null;
        String iconUrl = mUser.getBiggerProfileImageURL();
        ImageUtil.displayRoundedImage(iconUrl, mIconImage);

        mNameLabel.setText(mUser.getName());
        mScreenNameLabel.setText("@" + mUser.getScreenName());

        if (mUser.isProtected()) {
            mLockLabel.setVisibility(View.VISIBLE);
        }
        if (relationship.isSourceFollowedByTarget()) {
            mFollowedByLabel.setText(R.string.label_followed_by_target);
        }

        if (mUser.getId() == AccessTokenManager.getUserId()) {
            mFollowButton.setText(R.string.button_edit_profile);
        } else if (mFollowFlg) {
            mFollowButton.setText(R.string.button_unfollow);
        } else if (mBlocking) {
            mFollowButton.setText(R.string.button_blocking);
        } else {
            mFollowButton.setText(R.string.button_follow);
        }

        return v;
    }

    @OnClick(R.id.follow_botton)
    void followButtonClick() {
        if (mRuntimeFlg) {
            return;
        }
        if (mUser.getId() == AccessTokenManager.getUserId()) {
            Intent intent = new Intent(getActivity(), EditProfileActivity.class);
            intent.putExtra("user", mUser);
            startActivityForResult(intent, ProfileActivity.REQUEST_EDIT_PROFILE);
        } else if (mFollowFlg) {
            new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.confirm_unfollow)
                    .setPositiveButton(
                            R.string.button_unfollow,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    mRuntimeFlg = true;
                                    MessageUtil.showProgressDialog(getActivity(), getString(R.string.progress_process));
                                    DestroyFriendshipTask task = new DestroyFriendshipTask() {
                                        @Override
                                        protected void onPostExecute(Boolean success) {
                                            MessageUtil.dismissProgressDialog();
                                            if (success) {
                                                MessageUtil.showToast(R.string.toast_destroy_friendship_success);
                                                mFollowButton.setText(R.string.button_follow);
                                                mFollowFlg = false;
                                            } else {
                                                MessageUtil.showToast(R.string.toast_destroy_friendship_failure);
                                            }
                                            mRuntimeFlg = false;
                                        }
                                    };
                                    task.execute(mUser.getId());
                                }
                            }
                    )
                    .setNegativeButton(
                            R.string.button_cancel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }
                    )
                    .show();
        } else if (mBlocking) {
            new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.confirm_destroy_block)
                    .setPositiveButton(
                            R.string.button_destroy_block,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    mRuntimeFlg = true;
                                    MessageUtil.showProgressDialog(getActivity(), getString(R.string.progress_process));
                                    DestroyBlockTask task = new DestroyBlockTask() {
                                        @Override
                                        protected void onPostExecute(Boolean success) {
                                            MessageUtil.dismissProgressDialog();
                                            if (success) {
                                                MessageUtil.showToast(R.string.toast_destroy_block_success);
                                                mFollowButton.setText(R.string.button_follow);
                                                mBlocking = false;
                                            } else {
                                                MessageUtil.showToast(R.string.toast_destroy_block_failure);
                                            }
                                            mRuntimeFlg = false;
                                        }
                                    };
                                    task.execute(mUser.getId());
                                }
                            }
                    )
                    .setNegativeButton(
                            R.string.button_cancel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }
                    )
                    .show();
        } else {
            new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.confirm_follow)
                    .setPositiveButton(
                            R.string.button_follow,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    mRuntimeFlg = true;
                                    MessageUtil.showProgressDialog(getActivity(), getString(R.string.progress_process));
                                    FollowTask task = new FollowTask() {
                                        @Override
                                        protected void onPostExecute(Boolean success) {
                                            MessageUtil.dismissProgressDialog();
                                            if (success) {
                                                MessageUtil.showToast(R.string.toast_follow_success);
                                                mFollowButton.setText(R.string.button_unfollow);
                                                mFollowFlg = true;
                                            } else {
                                                MessageUtil.showToast(R.string.toast_follow_failure);
                                            }
                                            mRuntimeFlg = false;
                                        }
                                    };
                                    task.execute(mUser.getId());
                                }
                            }
                    )
                    .setNegativeButton(
                            R.string.button_cancel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }
                    )
                    .show();
        }
    }

    @OnClick(R.id.icon_image)
    void enlargeIcon() {
        Intent intent = new Intent(getActivity(), ScaleImageActivity.class);
        intent.putExtra("url", mUser.getOriginalProfileImageURL());
        startActivity(intent);
    }

    // TODO:
    @OnClick(R.id.list_button)
    void showListInfo() {

    }
}
