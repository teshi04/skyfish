package jp.tsur.twishi.task;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import jp.tsur.twishi.TwitterManager;
import twitter4j.Paging;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;


/**
 * リストに追加されているユーザのステータスを取得
 */
public class UserListStatusesLoader extends AsyncTaskLoader<ResponseList<Status>> {

    private long mMaxId;
    private long mUserListId;

    public UserListStatusesLoader(Context context, long maxId, long userListId) {
        super(context);
        mMaxId = maxId;
        mUserListId = userListId;
    }

    @Override
    public ResponseList<Status> loadInBackground() {

        try {
            Twitter twitter = TwitterManager.getTwitter();
            Paging paging = new Paging();
            if (mMaxId > 0) {
                paging.setMaxId(mMaxId - 1);
                paging.setCount(100);//BasicSettings.getPageCount());
            }
//            else {
//                ResponseList<User> members = twitter.getUserListMembers(mUserListId, 0);
//                for (User user : members) {
//                    mMembers.append(user.getId(), true);
//                }
//            }
            return twitter.getUserListStatuses(mUserListId, paging);
        } catch (OutOfMemoryError e) {
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}