package jp.tsur.twishi.ui.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.ButterKnife;
import butterknife.InjectView;
import jp.tsur.twishi.AccessTokenManager;
import jp.tsur.twishi.MyTextWatcher;
import jp.tsur.twishi.MyUncaughtExceptionHandler;
import jp.tsur.twishi.R;
import jp.tsur.twishi.TwitterManager;
import jp.tsur.twishi.adapter.MainPagerAdapter;
import jp.tsur.twishi.model.BasicSettings;
import jp.tsur.twishi.model.DrawerMenu;
import jp.tsur.twishi.ui.FavoritesListActivity;
import jp.tsur.twishi.ui.SearchActivity;
import jp.tsur.twishi.ui.SignInActivity;
import jp.tsur.twishi.ui.UserListsActivity;
import jp.tsur.twishi.ui.profile.ProfileActivity;
import jp.tsur.twishi.ui.settings.SettingsActivity;
import jp.tsur.twishi.ui.settings.TabSettingsActivity;
import jp.tsur.twishi.util.TwitterUtil;
import jp.tsur.twishi.widget.ClearEditText;
import twitter4j.Status;


public class MainActivity extends ActionBarActivity {

    private static final int REQUEST_TAB_SETTINGS = 100;
    private static final int REQUEST_SETTINGS = 300;
    private static final int REQUEST_SEARCH = 400;

    private static final Pattern USER_LIST_PATTERN = Pattern.compile("^(@[a-zA-Z0-9_]+)/(.*)$");
    private MainPagerAdapter mMainPagerAdapter;
    private boolean mFirstBoot = true;

    private static final int INDICATOR_OFFSET = 48;
    private int mIndicatorOffset;
    private ActionBarDrawerToggle mDrawerToggle;
    private Status mInReplyToStatus;

//    @InjectView(R.id.pager)
//    ViewPager mViewPager;
//    @InjectView(R.id.indicator)
//    View mIndicator;
//    @InjectView(R.id.track)
//    ViewGroup mTrack;
//    @InjectView(R.id.track_scroller)
//    HorizontalScrollView mTrackScroller;

    @InjectView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @InjectView(R.id.drawer_list)
    ListView mDrawerList;
    @InjectView(R.id.quick_tweet_container)
    LinearLayout mQuickTweetLayout;
    @InjectView(R.id.quick_tweet_edit)
    ClearEditText mQuickTweetEdit;
    @InjectView(R.id.count)
    TextView mCount;

//    @InjectView(R.id.send_button)
//    TextView mSendButton;

//    class ActionBarViewHolder {
//        @InjectView(R.id.action_bar_normal_layout)
//        LinearLayout mNormalLayout;
//        @InjectView(R.id.action_bar_search_layout)
//        FrameLayout mSearchLayout;
//        @InjectView(R.id.action_bar_search_text)
//        AutoCompleteEditText mSearchText;
//        @InjectView(R.id.action_bar_search_button)
//        TextView mSearchButton;
//        @InjectView(R.id.action_bar_search_cancel)
//        TextView mCancelButton;
//        @InjectView(R.id.action_bar_streaming_button)
//        TextView mStreamingButton;
//
//        public ActionBarViewHolder(View view) {
//            ButterKnife.inject(this, view);
//        }
//    }

    @Override
    protected void onStart() {
        super.onStart();
        MyUncaughtExceptionHandler.showBugReportDialogIfExist(this);

        if (BasicSettings.getKeepScreenOn()) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!AccessTokenManager.hasAccessToken()) {
            Intent intent = new Intent(this, SignInActivity.class);
            startActivity(intent);
            finish();
        }

        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);
//        ThemeUtil.setTheme(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        mDrawerToggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, R.string.open, R.string.close) {

            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        setupNavDrawerList();
        setup();
//        setupTab();

        mQuickTweetEdit.addTextChangedListener(mQuickTweetTextWatcher);
    }

    private void setupNavDrawerList() {
        // TODO: 一番上はアカウント切り替えのスピナーにする。
        String[] navigationIcons = getResources().getStringArray(R.array.drawer_icon);
        String[] navigationTitles = getResources().getStringArray(R.array.drawer_items);
        final ArrayList<DrawerMenu> drawerMenus = new ArrayList<>();
        for (int i = 0; i < navigationTitles.length; i++) {
            drawerMenus.add(new DrawerMenu(navigationIcons[i], navigationTitles[i]));
        }

        final DrawerMenuAdapter adapter = new DrawerMenuAdapter(this, R.layout.list_item_drawer_menu, drawerMenus);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                mDrawerLayout.closeDrawers();
                switch (position) {
                    case 0:
                        Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
                        intent.putExtra(ProfileActivity.EXTRA_USER_ID, AccessTokenManager.getUserId());
                        startActivity(intent);
                        break;
                    case 1:
                        intent = new Intent(MainActivity.this, SearchActivity.class);
                        startActivity(intent);
                        break;
                    case 2:
                        intent = new Intent(MainActivity.this, FavoritesListActivity.class);
                        intent.putExtra(ProfileActivity.EXTRA_USER_ID, AccessTokenManager.getUserId());
                        startActivity(intent);
                        break;
                    case 3:
                        intent = new Intent(MainActivity.this, UserListsActivity.class);
                        intent.putExtra(ProfileActivity.EXTRA_USER_ID, AccessTokenManager.getUserId());
                        startActivity(intent);
                        break;
                    case 5:
                        intent = new Intent(MainActivity.this, TabSettingsActivity.class);
                        startActivityForResult(intent, REQUEST_TAB_SETTINGS);
                        break;
                    case 6:
                        intent = SettingsActivity.getLaunchIntent(MainActivity.this);
                        startActivityForResult(intent, REQUEST_SETTINGS);
                    default:
                }
            }
        });
    }

    private void setup() {
//        final float density = getResources().getDisplayMetrics().density;
//        mIndicatorOffset = (int) (INDICATOR_OFFSET * density);
//
//        mMainPagerAdapter = new MainPagerAdapter(this, mViewPager);
//        mViewPager.setOffscreenPageLimit(10);
//        mViewPager.setOnPageChangeListener(new PageChangeListener());
//
        if (BasicSettings.getQuickMode()) {
//            showQuickPanel();
        }

        if (BasicSettings.getStreamingMode()) {
            TwitterManager.startStreaming();
        }
    }

    private void aa() {

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_TAB_SETTINGS:
                if (resultCode == RESULT_OK) {
//                    setupTab(); //todo
                }
                break;
//            case REQUEST_ACCOUNT_SETTING:
//                if (resultCode == RESULT_OK) {
//                    mSwitchAccessToken = (AccessToken) data.getSerializableExtra("accessToken");
//                }
//                if (mAccessTokenAdapter != null) {
//                    mAccessTokenAdapter.clear();
//                    for (AccessToken accessToken : AccessTokenManager.getAccessTokens()) {
//                        mAccessTokenAdapter.add(accessToken);
//                    }
//                }
//                break;
            case REQUEST_SETTINGS:
                if (resultCode == RESULT_OK) {
                    BasicSettings.init();
                    finish();
                    startActivity(new Intent(this, MainActivity.class));
                }
                break;
            case REQUEST_SEARCH:
                cancelSearch();
                break;
//            default:
//                break;
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        final ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;

        Matcher matcher = USER_LIST_PATTERN.matcher(title);
        if (matcher.find()) {
            actionBar.setTitle(matcher.group(2));
            actionBar.setSubtitle(matcher.group(1));
        } else {
            actionBar.setTitle(title);
            actionBar.setSubtitle("@" + AccessTokenManager.getScreenName());
        }
    }
//
//    @OnClick(R.id.fab)
//    void openPost() {
//        String statusText = mQuickTweetEdit.getText().toString();
//        DialogFragment dialogFragment =
//                UpdateStatusFragment.newInstance(mInReplyToStatus, statusText, statusText.length(), 0);
//        dialogFragment.show(getFragmentManager(), "update_status_dialog");
//    }
//
//    @OnClick(R.id.send_button)
//    void send() {
//        String msg = mQuickTweetEdit.getText().toString();
//        if (msg.length() > 0) {
//            MessageUtil.showProgressDialog(this, getString(R.string.progress_sending));
//
//            StatusUpdate statusUpdate = new StatusUpdate(msg);
//            if (mInReplyToStatus != null) {
//                statusUpdate.setInReplyToStatusId(mInReplyToStatus.getId());
//                mInReplyToStatus = null;
//            }
//
//            UpdateStatusTask task = new UpdateStatusTask() {
//                @Override
//                protected void onPostExecute(TwitterException e) {
//                    MessageUtil.dismissProgressDialog();
//                    if (e == null) {
//                        mQuickTweetEdit.setText("");
//                    } else if (e.getErrorCode() == TwitterUtil.ERROR_CODE_DUPLICATE_STATUS) {
//                        MessageUtil.showToast(getString(R.string.toast_update_status_already));
//                    } else {
//                        MessageUtil.showToast(R.string.toast_update_status_failure);
//                    }
//                }
//            };
//            task.execute(statusUpdate);
//        }
//    }

//    @OnLongClick(R.id.fab)
//    boolean toggleQuickTweet() {
//        if (mQuickTweetLayout.getVisibility() == View.VISIBLE) {
//            hideQuickPanel();
//        } else {
//            showQuickPanel();
//        }
//        return true;
//    }

    private void startSearch() {
        mDrawerToggle.setDrawerIndicatorEnabled(false);
//        mActionBarHolder.mNormalLayout.setVisibility(View.GONE);
//        mActionBarHolder.mSearchLayout.setVisibility(View.VISIBLE);
//        mActionBarHolder.mSearchText.showDropDown();
//        mActionBarHolder.mSearchText.setText("");
//        KeyboardUtil.showKeyboard(mActionBarHolder.mSearchText);
    }

    private void cancelSearch() {
//        mActionBarHolder.mSearchText.setText("");
//        KeyboardUtil.hideKeyboard(mActionBarHolder.mSearchText);
//        mActionBarHolder.mSearchLayout.setVisibility(View.GONE);
//        mActionBarHolder.mNormalLayout.setVisibility(View.VISIBLE);
        mDrawerToggle.setDrawerIndicatorEnabled(true);
    }
//
//    public void showQuickPanel() {
//        mQuickTweetLayout.setVisibility(View.VISIBLE);
//        mQuickTweetEdit.setFocusable(true);
//        mQuickTweetEdit.setFocusableInTouchMode(true);
//        mQuickTweetEdit.setEnabled(true);
//        BasicSettings.setQuickMod(true);
//    }

    //    public void hideQuickPanel() {
//        mQuickTweetEdit.setFocusable(false);
//        mQuickTweetEdit.setFocusableInTouchMode(false);
//        mQuickTweetEdit.setEnabled(false);
//        mQuickTweetEdit.clearFocus();
//        mQuickTweetLayout.setVisibility(View.GONE);
//        mInReplyToStatus = null;
//        BasicSettings.setQuickMod(false);
//    }
    private TextWatcher mQuickTweetTextWatcher = new MyTextWatcher() {

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            int textColor;
            int length = TwitterUtil.count(charSequence.toString());
            // 140文字をオーバーした時は文字数を赤色に
            if (length < 0) {
                textColor = getResources().getColor(R.color.holo_red_light);
            } else {
                textColor = getResources().getColor(R.color.text_color);
            }
//            mCount.setTextColor(textColor);
//            mCount.setText(String.valueOf(length));

//            if (length < 0 || length == 140) {
//                // 文字数が0文字または140文字以上の時はボタンを無効
//                mSendButton.setEnabled(false);
//            } else {
//                mSendButton.setEnabled(true);
//            }
        }
    };

    /**
     * drawerメニュー
     */
    public class DrawerMenuAdapter extends ArrayAdapter<DrawerMenu> {

        private LayoutInflater mInflater;
        private int mLayout;

        public DrawerMenuAdapter(Context context, int resource, List<DrawerMenu> menuLabelList) {
            super(context, resource, menuLabelList);
            mInflater = LayoutInflater.from(context);
            mLayout = resource;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View view = convertView;
            if (view == null) {
                view = mInflater.inflate(this.mLayout, null);
            }

            final DrawerMenu menu = getItem(position);

            assert view != null;
            ((TextView) view.findViewById(R.id.menu_img)).setText(menu.getIcon());
            ((TextView) view.findViewById(R.id.menu_label)).setText(menu.getLabel());

            return view;
        }
    }
//
//    private AdapterView.OnItemClickListener getActionBarAutoCompleteOnClickListener() {
//        return new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                if (mActionBarHolder.mSearchText.getText() == null) {
//                    return;
//                }
//                Intent intent;
//                String searchWord = mActionBarHolder.mSearchText.getString();
//                KeyboardUtil.hideKeyboard(mActionBarHolder.mSearchText);
//                if (mSearchAdapter.isSavedMode()) {
//                    intent = new Intent(MainActivity.this, SearchActivity.class);
//                    intent.putExtra(SearchActivity.EXTRA_QUERY, searchWord);
//                    startActivityForResult(intent, REQUEST_SEARCH);
//                    return;
//                }
//                switch (i) {
//                    case 0:
//                        intent = new Intent(MainActivity.this, SearchActivity.class);
//                        intent.putExtra(SearchActivity.EXTRA_QUERY, searchWord);
//                        startActivityForResult(intent, REQUEST_SEARCH);
//                        break;
//                    case 1:
////                        intent = new Intent(MainActivity.this, UserSearchActivity.class);
////                        intent.putExtra("query", searchWord);
////                        startActivityForResult(intent, REQUEST_SEARCH);
////                        break;
//                    case 2:
//                        intent = new Intent(MainActivity.this, ProfileActivity.class);
//                        intent.putExtra(ProfileActivity.EXTRA_SCREEN_NAME, searchWord);
//                        startActivity(intent);
//                        break;
//                }
//            }
//        };
//    }

//    // エンターキーで検索する
//    private View.OnKeyListener getOnKeyListener() {
//        return new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                if (event.getAction() == KeyEvent.ACTION_DOWN
//                        && keyCode == KeyEvent.KEYCODE_ENTER) {
//                    if (mActionBarHolder.mSearchText.getText() == null) {
//                        return false;
//                    }
//                    Intent intent;
//                    String searchWord = mActionBarHolder.mSearchText.getString();
//                    KeyboardUtil.hideKeyboard(mActionBarHolder.mSearchText);
//                    intent = new Intent(MainActivity.this, SearchActivity.class);
//                    intent.putExtra(SearchActivity.EXTRA_QUERY, searchWord);
//                    startActivityForResult(intent, REQUEST_SEARCH);
//                    return true;
//                }
//                return false;
//            }
//        };
//    }

//    /**
//     * リプや引用などのツイート要求、クイックモードでない場合はPostActivityへ
//     */
//    @SuppressWarnings("UnusedDeclaration")
//    public void onEventMainThread(OpenEditorEvent event) {
//        View singleLineTweet = findViewById(R.id.quick_tweet_container);
//        if (singleLineTweet != null && singleLineTweet.getVisibility() == View.VISIBLE) {
//            mQuickTweetEdit.setText(event.getText());
//            if (event.getSelection() != null) {
//                if (event.getSelectionEnd() != null) {
//                    mQuickTweetEdit.setSelection(event.getSelection(), event.getSelectionEnd());
//                } else {
//                    mQuickTweetEdit.setSelection(event.getSelection());
//                }
//            }
//            mInReplyToStatus = event.getInReplyToStatus();
//            KeyboardUtil.showKeyboard(mQuickTweetEdit);
//        } else {
//            DialogFragment dialogFragment =
//                    UpdateStatusFragment.newInstance(null, event.getText(), event.getSelection(), event.getSelectionEnd());
//            dialogFragment.show(getFragmentManager(), "update_status_dialog");
//        }
//    }
//

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

//    @Override
//    protected void onPause() {
//        TwitterManager.pauseStreaming();
////        EventBus.getDefault().unregister(this);
//        super.onPause();
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
////        EventBus.getDefault().register(this);
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case android.R.id.home:
                cancelSearch();
                return true;
        }
        return false;
    }

}