package jp.tsur.twishi.event;

public class StreamingConnectionEvent {

    public static enum Status {
        STREAMING_CONNECT,
        STREAMING_CLEANUP,
        STREAMING_DISCONNECT
    }

    private Status mStatus;

    public StreamingConnectionEvent(Status status) {
        mStatus = status;
    }

    public static StreamingConnectionEvent onConnect() {
        return new StreamingConnectionEvent(Status.STREAMING_CONNECT);
    }

    public static StreamingConnectionEvent onCleanUp() {
        return new StreamingConnectionEvent(Status.STREAMING_CLEANUP);
    }

    public static StreamingConnectionEvent onDisconnect() {
        return new StreamingConnectionEvent(Status.STREAMING_DISCONNECT);
    }

    public Status getStatus() {
        return mStatus;
    }
}
