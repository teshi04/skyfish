package jp.tsur.twishi.task;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import jp.tsur.twishi.TwitterManager;
import twitter4j.Status;


/**
 * ステータスを取得
 */
public class StatusLoader extends AsyncTaskLoader<Status> {

    private long mStatusId;

    public StatusLoader(Context context, long statusId) {
        super(context);
        mStatusId = statusId;
    }

    @Override
    public Status loadInBackground() {
        try {
            return TwitterManager.getTwitter().showStatus(mStatusId);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}