package jp.tsur.twishi.ui.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import jp.tsur.twishi.R;
import jp.tsur.twishi.TabManager;
import jp.tsur.twishi.TwitterManager;
import jp.tsur.twishi.adapter.MainPagerAdapter;
import jp.tsur.twishi.event.AlertDialogEvent;
import jp.tsur.twishi.event.GoToTopEvent;
import jp.tsur.twishi.event.NewRecordEvent;
import jp.tsur.twishi.event.StreamingConnectionEvent;
import jp.tsur.twishi.model.BasicSettings;


public class TimelineFragment extends Fragment {

    private MainPagerAdapter mMainPagerAdapter;

    private static final int INDICATOR_OFFSET = 48;
    private int mIndicatorOffset;

    @InjectView(R.id.pager)
    ViewPager mViewPager;
    @InjectView(R.id.indicator)
    View mIndicator;
    @InjectView(R.id.track)
    ViewGroup mTrack;
    @InjectView(R.id.track_scroller)
    HorizontalScrollView mTrackScroller;
//    @InjectView(R.id.count)
//    TextView mCount;
//__
//    @InjectView(R.id.drawer_layout)
//    DrawerLayout mDrawerLayout;
//    @InjectView(R.id.drawer_list)
//    ListView mDrawerList;
//    @InjectView(R.id.quick_tweet_container)
//    LinearLayout mQuickTweetLayout;
//    @InjectView(R.id.quick_tweet_edit)
//    ClearEditText mQuickTweetEdit;
//
//    @InjectView(R.id.send_button)
//    TextView mSendButton;

//    class ActionBarViewHolder {
//        @InjectView(R.id.action_bar_normal_layout)
//        LinearLayout mNormalLayout;
//        @InjectView(R.id.action_bar_search_layout)
//        FrameLayout mSearchLayout;
//        @InjectView(R.id.action_bar_search_text)
//        AutoCompleteEditText mSearchText;
//        @InjectView(R.id.action_bar_search_button)
//        TextView mSearchButton;
//        @InjectView(R.id.action_bar_search_cancel)
//        TextView mCancelButton;
//        @InjectView(R.id.action_bar_streaming_button)
//        TextView mStreamingButton;

//        @OnClick(R.id.action_bar_search_button)
//        void actionBarSearchButton() {
//            startSearch();
//        }
//
//        @OnClick(R.id.action_bar_search_cancel)
//        void actionBarCancelButton() {
//            cancelSearch();
//        }
//
//        @OnClick(R.id.action_bar_streaming_button)
//        void actionBarToggleStreaming() {
//            final boolean turnOn = !BasicSettings.getStreamingMode();
//            android.support.v4.app.DialogFragment dialog = StreamingSwitchDialogFragment.newInstance(turnOn);
//            dialog.show(getFragmentManager(), "dialog");
//        }
//
//        public ActionBarViewHolder(View view) {
//            ButterKnife.inject(this, view);
//        }
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.inject(this, view);
        setup();
        setupTab();
    }

    //    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);

//        ThemeUtil.setTheme(this);
//        setContentView(R.layout.activity_main);
//
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//
//        if (toolbar != null) {
//            setSupportActionBar(toolbar);
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//            getSupportActionBar().setDisplayShowHomeEnabled(true);
//        }

//        mDrawerToggle = new ActionBarDrawerToggle(
//                this, mDrawerLayout, R.string.open, R.string.close) {
//
//            public void onDrawerClosed(View view) {
//                invalidateOptionsMenu();
//            }
//
//            public void onDrawerOpened(View drawerView) {
//                invalidateOptionsMenu();
//            }
//        };
//        mDrawerLayout.setDrawerListener(mDrawerToggle);

//        setupNavDrawerList();
//        setup();
//        setupTab();

//        mFloatingActionButton.setDrawable(getResources().getDrawable(R.drawable.enpitu));
//        mQuickTweetEdit.addTextChangedListener(mQuickTweetTextWatcher);
//    }


    private void setup() {
        final float density = getResources().getDisplayMetrics().density;
        mIndicatorOffset = (int) (INDICATOR_OFFSET * density);

        FragmentManager fm = getFragmentManager();
        mMainPagerAdapter = new MainPagerAdapter(getActivity(), mViewPager);
        mViewPager.setOffscreenPageLimit(10);
        mViewPager.setOnPageChangeListener(new PageChangeListener());
//        if (BasicSettings.getQuickMode()) {
//            showQuickPanel();
//        }
//
//        if (BasicSettings.getStreamingMode()) {
//            TwitterManager.startStreaming();
//        }
    }

    private void setupTab() {
        ArrayList<TabManager.Tab> tabs = TabManager.loadTabs();
        if (tabs.size() > 0) {
            mTrack.removeAllViews();
            mMainPagerAdapter.clearTab();

            LayoutInflater inflater = LayoutInflater.from(getActivity());
            int position = 0;
            for (TabManager.Tab tab : tabs) {
                // タブをコンテナに追加
                TextView tv = (TextView) inflater.inflate(R.layout.tab_item, mTrack, false);
                tv.setTag(position++);
                tv.setText(tab.getIcon());
                tv.setOnClickListener(mMenuOnClickListener);
//                tv.setOnLongClickListener(mMenuOnLongClickListener);
                mTrack.addView(tv);

                if (tab.id == TabManager.TIMELINE_TAB_ID) {
                    mMainPagerAdapter.addTab(MainTimelineFragment.class, null, tab.getName(), tab.id);
                } else if (tab.id == TabManager.INTERACTIONS_TAB_ID) {
                    mMainPagerAdapter.addTab(InteractionsFragment.class, null, tab.getName(), tab.id);
                } else if (tab.id == TabManager.DIRECT_MESSAGES_TAB_ID) {
                    mMainPagerAdapter.addTab(DirectMessagesFragment.class, null, tab.getName(), tab.id);
                } else {
                    Bundle args = new Bundle();
                    args.putLong(UserListTimelineFragment.EXTRA_USER_LIST_ID, tab.id);
                    mMainPagerAdapter.addTab(UserListTimelineFragment.class, args, tab.getName(), tab.id);
                }
            }
            mMainPagerAdapter.notifyDataSetChanged();
        }
    }


//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        switch (requestCode) {
//            case REQUEST_TAB_SETTINGS:
//                if (resultCode == RESULT_OK) {
//                    setupTab();
//                }
//                break;
////            case REQUEST_ACCOUNT_SETTING:
////                if (resultCode == RESULT_OK) {
////                    mSwitchAccessToken = (AccessToken) data.getSerializableExtra("accessToken");
////                }
////                if (mAccessTokenAdapter != null) {
////                    mAccessTokenAdapter.clear();
////                    for (AccessToken accessToken : AccessTokenManager.getAccessTokens()) {
////                        mAccessTokenAdapter.add(accessToken);
////                    }
////                }
////                break;
//            case REQUEST_SETTINGS:
//                if (resultCode == RESULT_OK) {
//                    BasicSettings.init();
//                    finish();
//                    startActivity(new Intent(this, MainActivity.class));
//                }
//                break;
////            case REQUEST_SEARCH:
////                cancelSearch();
//                break;
////            default:
////                break;
//        }
//    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }


//    @Override
//    protected void onPostResume() {
//        super.onPostResume();
//
//        if (mFirstBoot) {
//            mFirstBoot = false;
//            return;
//        }

    // 初回起動
//        BasicSettings.init();
//        BasicSettings.resetNotification();

//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                // フォントサイズの変更や他のアクティビティでのfav/RTを反映
//                try {
//                    mMainPagerAdapter.notifyDataSetChanged();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, 1000);
////
//        if (mSwitchAccessToken != null) {
//            TwitterManager.switchAccessToken(mSwitchAccessToken);
//            mSwitchAccessToken = null;
//        }
//        TwitterManager.resumeStreaming();
//        if (TwitterManager.getTwitterStreamConnected()) {
//            mActionBarHolder.mStreamingButton.setTextColor(getResources().getColor(R.color.holo_green_light));
//        } else {
//            if (BasicSettings.getStreamingMode()) {
//                mActionBarHolder.mStreamingButton.setTextColor(getResources().getColor(R.color.holo_red_light));
//            } else {
//                mActionBarHolder.mStreamingButton.setTextColor(getResources().getColor(R.color.white));
//            }
//        }
//    }

    @Override
    public void onPause() {
        TwitterManager.pauseStreaming();
        EventBus.getDefault().unregister(this);
        super.onPause();
    }


//    @Override
//    public void onConfigurationChanged(Configuration newConfig) {
//        mDrawerToggle.onConfigurationChanged(newConfig);
//    }

//    @Override
//    public void setTitle(CharSequence title) {
//        final ActionBar actionBar = getSupportActionBar();
//        assert actionBar != null;
//
//        Matcher matcher = USER_LIST_PATTERN.matcher(title);
//        if (matcher.find()) {
//            actionBar.setTitle(matcher.group(2));
//            actionBar.setSubtitle(matcher.group(1));
//        } else {
//            actionBar.setTitle(title);
//            actionBar.setSubtitle("@" + AccessTokenManager.getScreenName());
//        }
//    }

//    @OnClick(R.id.fab)
//    void openPost() {
//        String statusText = mQuickTweetEdit.getText().toString();
//        DialogFragment dialogFragment =
//                UpdateStatusFragment.newInstance(mInReplyToStatus, statusText, statusText.length(), 0);
//        dialogFragment.show(getFragmentManager(), "update_status_dialog");
//    }
//
//    @OnClick(R.id.send_button)
//    void send() {
//        String msg = mQuickTweetEdit.getText().toString();
//        if (msg.length() > 0) {
//            MessageUtil.showProgressDialog(this, getString(R.string.progress_sending));
//
//            StatusUpdate statusUpdate = new StatusUpdate(msg);
//            if (mInReplyToStatus != null) {
//                statusUpdate.setInReplyToStatusId(mInReplyToStatus.getId());
//                mInReplyToStatus = null;
//            }
//
//            UpdateStatusTask task = new UpdateStatusTask() {
//                @Override
//                protected void onPostExecute(TwitterException e) {
//                    MessageUtil.dismissProgressDialog();
//                    if (e == null) {
//                        mQuickTweetEdit.setText("");
//                    } else if (e.getErrorCode() == TwitterUtil.ERROR_CODE_DUPLICATE_STATUS) {
//                        MessageUtil.showToast(getString(R.string.toast_update_status_already));
//                    } else {
//                        MessageUtil.showToast(R.string.toast_update_status_failure);
//                    }
//                }
//            };
//            task.execute(statusUpdate);
//        }
//    }

//    @OnLongClick(R.id.fab)
//    boolean toggleQuickTweet() {
//        if (mQuickTweetLayout.getVisibility() == View.VISIBLE) {
//            hideQuickPanel();
//        } else {
//            showQuickPanel();
//        }
//        return true;
//    }

//    private void startSearch() {
//        mDrawerToggle.setDrawerIndicatorEnabled(false);
//        mActionBarHolder.mNormalLayout.setVisibility(View.GONE);
//        mActionBarHolder.mSearchLayout.setVisibility(View.VISIBLE);
//        mActionBarHolder.mSearchText.showDropDown();
//        mActionBarHolder.mSearchText.setText("");
//        KeyboardUtil.showKeyboard(mActionBarHolder.mSearchText);
//    }
//
//    private void cancelSearch() {
//        mActionBarHolder.mSearchText.setText("");
//        KeyboardUtil.hideKeyboard(mActionBarHolder.mSearchText);
//        mActionBarHolder.mSearchLayout.setVisibility(View.GONE);
//        mActionBarHolder.mNormalLayout.setVisibility(View.VISIBLE);
//        mDrawerToggle.setDrawerIndicatorEnabled(true);
//    }

    public void showTopView() {
        TextView tv = (TextView) mTrack.getChildAt(mViewPager.getCurrentItem());
        if (tv != null) {
            tv.setTextColor(getResources().getColor(R.color.text_color));
//            ThemeUtil.setThemeTextColor(button, R.attr.menu_text_color);
        }
    }

//    public void showQuickPanel() {
//        mQuickTweetLayout.setVisibility(View.VISIBLE);
//        mQuickTweetEdit.setFocusable(true);
//        mQuickTweetEdit.setFocusableInTouchMode(true);
//        mQuickTweetEdit.setEnabled(true);
//        BasicSettings.setQuickMod(true);
//    }
//
//    public void hideQuickPanel() {
//        mQuickTweetEdit.setFocusable(false);
//        mQuickTweetEdit.setFocusableInTouchMode(false);
//        mQuickTweetEdit.setEnabled(false);
//        mQuickTweetEdit.clearFocus();
//        mQuickTweetLayout.setVisibility(View.GONE);
//        mInReplyToStatus = null;
//        BasicSettings.setQuickMod(false);
//    }

    private class PageChangeListener implements ViewPager.OnPageChangeListener {
        private int mScrollingState = ViewPager.SCROLL_STATE_IDLE;

        @Override
        public void onPageSelected(int position) {
            // スクロール中はonPageScrolled()で描画するのでここではしない
            if (mScrollingState == ViewPager.SCROLL_STATE_IDLE) {
                updateIndicatorPosition(position, 0);
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {
            mScrollingState = state;
        }

        @Override
        public void onPageScrolled(int position, float positionOffset,
                                   int positionOffsetPixels) {
            updateIndicatorPosition(position, positionOffset);
        }

        private void updateIndicatorPosition(int position, float positionOffset) {
//  TODO:         setTitle(mMainPagerAdapter.getPageTitle(position));
//            BaseMainTlFragment f = mMainPagerAdapter.findFragmentByPosition(position);
//            if (f.isTop()) {
//                showTopView();
//            }

            // 現在の位置のタブのView
            final View view = mTrack.getChildAt(position);
            // 現在の位置の次のタブのView、現在の位置が最後のタブのときはnull
            final View view2 = position == (mTrack.getChildCount() - 1) ? null
                    : mTrack.getChildAt(position + 1);

            int left = view.getLeft();

            // 現在の位置のタブの横幅
            int width = view.getWidth();
            // 現在の位置の次のタブの横幅
            int width2 = view2 == null ? width : view2.getWidth();

            // インディケータの幅
            int indicatorWidth = (int) (width2 * positionOffset + width
                    * (1 - positionOffset));
            // インディケータの左端の位置
            int indicatorLeft = (int) (left + positionOffset * width);

            // インディケータの幅と左端の位置をセット
            final FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mIndicator
                    .getLayoutParams();
            layoutParams.width = indicatorWidth;
            layoutParams.setMargins(indicatorLeft, 0, 0, 0);
            mIndicator.setLayoutParams(layoutParams);

            // インディケータが画面に入るように、タブの領域をスクロール
            mTrackScroller.scrollTo(indicatorLeft - mIndicatorOffset, 0);
        }
    }

    /**
     * メニューをタップしたらページ移動（見ているページのメニューだったら一番上へスクロール）
     */
    private View.OnClickListener mMenuOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int position = (Integer) view.getTag();
//            BaseMainTlFragment f = mMainPagerAdapter.findFragmentByPosition(position);
//            if (f == null) {
//                return;
//            }
//            if (mViewPager.getCurrentItem() != position) {
//                mViewPager.setCurrentItem(position);
//            } else {
//                if (f.goToTop()) {
//                    showTopView();
//                }
//            }
        }
    };

    /**
     * メニューをロングタップしたらリロード
     */
    private View.OnLongClickListener mMenuOnLongClickListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View view) {
            int position = (Integer) view.getTag();
            BaseMainTlFragment f = mMainPagerAdapter.findFragmentByPosition(position);
            if (f == null) {
                return false;
            }
            f.reload();
            return true;
        }
    };

//    private TextWatcher mQuickTweetTextWatcher = new MyTextWatcher() {
//
//        @Override
//        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
//            int textColor;
//            int length = TwitterUtil.count(charSequence.toString());
//            // 140文字をオーバーした時は文字数を赤色に
//            if (length < 0) {
//                textColor = getResources().getColor(R.color.holo_red_light);
//            } else {
//                textColor = getResources().getColor(R.color.text_color);
//            }
//            mCount.setTextColor(textColor);
//            mCount.setText(String.valueOf(length));
//
//            if (length < 0 || length == 140) {
//                // 文字数が0文字または140文字以上の時はボタンを無効
//                mSendButton.setEnabled(false);
//            } else {
//                mSendButton.setEnabled(true);
//            }
//        }
//    };

//    /**
//     * drawerメニュー
//     */
//    public class DrawerMenuAdapter extends ArrayAdapter<DrawerMenu> {
//
//        private LayoutInflater mInflater;
//        private int mLayout;
//
//        public DrawerMenuAdapter(Context context, int resource, List<DrawerMenu> menuLabelList) {
//            super(context, resource, menuLabelList);
//            mInflater = LayoutInflater.from(context);
//            mLayout = resource;
//        }
//
//        @Override
//        public View getView(final int position, View convertView, ViewGroup parent) {
//
//            View view = convertView;
//            if (view == null) {
//                view = mInflater.inflate(this.mLayout, null);
//            }
//
//            final DrawerMenu menu = getItem(position);
//
//            assert view != null;
//            ((TextView) view.findViewById(R.id.menu_img)).setText(menu.getIcon());
//            ((TextView) view.findViewById(R.id.menu_label)).setText(menu.getLabel());
//
//            return view;
//        }
//    }


    /**
     * ダイアログ表示要求
     */
    @SuppressWarnings("UnusedDeclaration")
    public void onEventMainThread(AlertDialogEvent event) {
//        event.getDialogFragment().show(getSupportFragmentManager(), "dialog");
    }

    /**
     * タイムラインなど一番上まで見たという合図
     */
    @SuppressWarnings("UnusedDeclaration")
    public void onEventMainThread(GoToTopEvent event) {
        showTopView();
    }

    //TODO
//    /**
//     * リプや引用などのツイート要求、クイックモードでない場合はPostActivityへ
//     */
//    @SuppressWarnings("UnusedDeclaration")
//    public void onEventMainThread(OpenEditorEvent event) {
//        View singleLineTweet = findViewById(R.id.quick_tweet_container);
//        if (singleLineTweet != null && singleLineTweet.getVisibility() == View.VISIBLE) {
//            mQuickTweetEdit.setText(event.getText());
//            if (event.getSelection() != null) {
//                if (event.getSelectionEnd() != null) {
//                    mQuickTweetEdit.setSelection(event.getSelection(), event.getSelectionEnd());
//                } else {
//                    mQuickTweetEdit.setSelection(event.getSelection());
//                }
//            }
//            mInReplyToStatus = event.getInReplyToStatus();
//            KeyboardUtil.showKeyboard(mQuickTweetEdit);
//        } else {
//            DialogFragment dialogFragment =
//                    UpdateStatusFragment.newInstance(null, event.getText(), event.getSelection(), event.getSelectionEnd());
//            dialogFragment.show(getFragmentManager(), "update_status_dialog");
//        }
//    }

    /**
     * ストリーミングAPI接続
     */
    @SuppressWarnings("UnusedDeclaration")
    public void onEventMainThread(StreamingConnectionEvent event) {
        if (BasicSettings.getStreamingMode()) {
            switch (event.getStatus()) {
                case STREAMING_CONNECT:
//                    mActionBarHolder.mStreamingButton.setTextColor(getResources().getColor(R.color.holo_green_light));
                    break;
                case STREAMING_CLEANUP:
//                    mActionBarHolder.mStreamingButton.setTextColor(getResources().getColor(R.color.holo_orange_light));
                    break;
                case STREAMING_DISCONNECT:
//                    mActionBarHolder.mStreamingButton.setTextColor(getResources().getColor(R.color.holo_red_light));
                    break;
            }
        } else {
//            mActionBarHolder.mStreamingButton.setTextColor(getResources().getColor(R.color.white));
        }
    }

//    /**
//     * アカウント変更
//     */
//    @SuppressWarnings("UnusedDeclaration")
//    public void onEventMainThread(AccountChangeEvent event) {
////        if (mAccessTokenAdapter != null) {
////            mAccessTokenAdapter.notifyDataSetChanged();
////        }
////        setupTab();
////        mViewPager.setCurrentItem(0);
////        EventBus.getDefault().post(new PostAccountChangeEvent(mMainPagerAdapter.getItemId(mViewPager.getCurrentItem())));
//    }

    /**
     * ストリーミングで新しいツイートを受信
     * オートスクロールじゃない場合は対応するタブを青くする
     */
    @SuppressWarnings("UnusedDeclaration")
    public void onEventMainThread(NewRecordEvent event) {
        int position = mMainPagerAdapter.findPositionById(event.getTabId());
        if (position < 0) {
            return;
        }
        TextView tv = (TextView) mTrack.getChildAt(position);
        if (tv == null) {
            return;
        }
        if (mViewPager.getCurrentItem() == position && event.getAutoScroll()) {
            tv.setTextColor(getResources().getColor(R.color.text_color));
//            ThemeUtil.setThemeTextColor(button, R.attr.menu_text_color);
        } else {
            tv.setTextColor(getResources().getColor(R.color.twitter_primary));
//            ThemeUtil.setThemeTextColor(button, R.attr.holo_blue);
        }
    }
}

