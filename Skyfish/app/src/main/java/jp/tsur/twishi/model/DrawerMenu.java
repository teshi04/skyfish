package jp.tsur.twishi.model;

/**
 * Created by teshi on 2014/05/22.
 */
public class DrawerMenu {

    public DrawerMenu(String icon, String label) {
        this.icon = icon;
        this.label = label;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    private String icon;
    private String label;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
