package jp.tsur.twishi.adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import jp.tsur.twishi.AccessTokenManager;
import jp.tsur.twishi.R;
import jp.tsur.twishi.event.AlertDialogEvent;
import jp.tsur.twishi.event.DestroyUserListEvent;
import jp.tsur.twishi.model.UserListCache;
import jp.tsur.twishi.model.UserListWithRegistered;
import jp.tsur.twishi.task.DestroyUserListSubscriptionTask;
import jp.tsur.twishi.task.DestroyUserListTask;
import jp.tsur.twishi.util.MessageUtil;
import jp.tsur.twishi.widget.FontelloTextView;
import twitter4j.UserList;

public class SubscribeUserListAdapter extends ArrayAdapter<UserListWithRegistered> {

    private LayoutInflater mInflater;
    private int mLayout;

    static class ViewHolder {
        @InjectView(R.id.checkbox)
        CheckBox mCheckBox;
        @InjectView(R.id.trash)
        FontelloTextView mTrash;
        @InjectView(R.id.details_label)
        TextView mDetailsLabel;
        @InjectView(R.id.lock_label)
        TextView mLockLabel;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }

    public SubscribeUserListAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mLayout = textViewResourceId;
    }

    public UserListWithRegistered findByUserListId(long userListId) {
        for (int i = 0; i < getCount(); i++) {
            UserListWithRegistered userListWithRegistered = getItem(i);
            if (userListWithRegistered.getUserList().getId() == userListId) {
                return userListWithRegistered;
            }
        }
        return null;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder viewHolder;
        if (view == null) {
            view = mInflater.inflate(this.mLayout, null);
            if (view == null) {
                return null;
            }
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        final UserListWithRegistered userListWithRegistered = getItem(position);
        final UserList userList = userListWithRegistered.getUserList();

        viewHolder.mTrash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AccessTokenManager.getUserId() == userList.getUser().getId()) {
                    // 自分のリストの場合はリスト削除
                    DialogFragment dialog = DestroyUserListDialogFragment.newInstance(userList);
                    EventBus.getDefault().post(new AlertDialogEvent(dialog));
                } else {
                    // 他人のリストの場合はリストの購読解除
                    DialogFragment dialog = DestroyUserListSubscriptionDialogFragment.newInstance(userList);
                    EventBus.getDefault().post(new AlertDialogEvent(dialog));
                }
            }
        });

        if (AccessTokenManager.getUserId() == userList.getUser().getId()) {
            viewHolder.mCheckBox.setText(userList.getName());
        } else {
            viewHolder.mCheckBox.setText(userList.getFullName());
        }
        viewHolder.mDetailsLabel.setText(getContext().getString(R.string.label_members, userList.getMemberCount()));
        if (userList.isPublic()) {
            viewHolder.mLockLabel.setVisibility(View.GONE);
        } else {
            viewHolder.mLockLabel.setVisibility(View.VISIBLE);
        }
        viewHolder.mCheckBox.setOnCheckedChangeListener(null);
        viewHolder.mCheckBox.setChecked(userListWithRegistered.isRegistered());
        viewHolder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                userListWithRegistered.setRegistered(b);
            }
        });

        return view;
    }

    /**
     * リストの削除をするか確認するダイアログ
     */
    public static final class DestroyUserListDialogFragment extends DialogFragment {

        public static DestroyUserListDialogFragment newInstance(UserList userList) {
            DestroyUserListDialogFragment f = new DestroyUserListDialogFragment();
            Bundle args = new Bundle();
            args.putSerializable("user_list", userList);
            f.setArguments(args);
            return f;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final UserList userList = (UserList) getArguments().getSerializable("user_list");
            if (userList == null) {
                return null;
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.confirm_destroy_user_list);
            builder.setMessage(getString(R.string.confirm_destroy_user_list2, userList.getName()));
            builder.setPositiveButton(R.string.confirm_destroy_user_list,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            MessageUtil.showProgressDialog(getActivity(), getString(R.string.progress_process));
                            new DestroyUserListTask() {
                                @Override
                                protected void onPostExecute(Boolean success) {
                                    MessageUtil.dismissProgressDialog();
                                    if (success) {
                                        MessageUtil.showToast(R.string.toast_destroy_user_list_success);
                                        EventBus.getDefault().post(new DestroyUserListEvent(userList.getId()));
                                        UserListCache.getUserLists().remove(userList);
                                    } else {
                                        MessageUtil.showToast(R.string.toast_destroy_user_list_failure);
                                    }
                                }
                            }.execute(userList.getId());

                            dismiss();
                        }
                    }
            );
            builder.setNegativeButton(R.string.button_cancel,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dismiss();
                        }
                    }
            );
            return builder.create();
        }
    }

    public static final class DestroyUserListSubscriptionDialogFragment extends DialogFragment {

        public static DestroyUserListSubscriptionDialogFragment newInstance(UserList userList) {
            DestroyUserListSubscriptionDialogFragment f = new DestroyUserListSubscriptionDialogFragment();
            Bundle args = new Bundle();
            args.putSerializable("user_list", userList);
            f.setArguments(args);
            return f;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final UserList userList = (UserList) getArguments().getSerializable("user_list");
            if (userList == null) {
                return null;
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.confirm_destroy_user_list_subscribe);
            builder.setMessage(getString(R.string.confirm_destroy_user_list_subscribe2, userList.getName()));
            builder.setPositiveButton(R.string.confirm_destroy_user_list_subscribe,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            MessageUtil.showProgressDialog(getActivity(), getString(R.string.progress_process));
                            new DestroyUserListSubscriptionTask() {
                                @Override
                                protected void onPostExecute(Boolean success) {
                                    MessageUtil.dismissProgressDialog();
                                    if (success) {
                                        MessageUtil.showToast(R.string.toast_destroy_user_list_subscription_success);
                                        EventBus.getDefault().post(new DestroyUserListEvent(userList.getId()));
                                        UserListCache.getUserLists().remove(userList);
                                    } else {
                                        MessageUtil.showToast(R.string.toast_destroy_user_list_subscription_failure);
                                    }
                                }
                            }.execute(userList.getId());

                            dismiss();
                        }
                    }
            );
            builder.setNegativeButton(R.string.button_cancel,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dismiss();
                        }
                    }
            );
            return builder.create();
        }
    }
}
