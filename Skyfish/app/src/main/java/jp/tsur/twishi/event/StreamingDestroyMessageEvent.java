package jp.tsur.twishi.event;

public class StreamingDestroyMessageEvent {

    private final long mStatusId;

    public StreamingDestroyMessageEvent(final long statusId) {
        mStatusId = statusId;
    }

    public long getStatusId() {
        return mStatusId;
    }
}
