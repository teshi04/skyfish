package jp.tsur.twishi.ui.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;

import jp.tsur.twishi.R;
import jp.tsur.twishi.adapter.UserAdapter;
import jp.tsur.twishi.task.FollowersListLoader;
import twitter4j.PagableResponseList;
import twitter4j.User;

/**
 * フォロワーリスト
 */
public class FollowersListFragment extends ListFragment implements LoaderManager.LoaderCallbacks<PagableResponseList<User>> {

    private UserAdapter mAdapter;
    private long mUserId;
    private long mCursor;
    private ListView mListView;
    private View mFooter;
    private boolean mAutoLoader;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mListView = getListView();

        LayoutInflater inflater = LayoutInflater.from(getActivity());
        mFooter = inflater.inflate(R.layout.guruguru, mListView, false);
        mListView.addFooterView(mFooter, null, false);
        mAdapter = new UserAdapter(getActivity(), R.layout.list_item_user);
        setListAdapter(mAdapter);
        setListShown(false);

        mUserId = getArguments().getLong(ProfileActivity.EXTRA_USER_ID);

        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (totalItemCount == firstVisibleItem + visibleItemCount) {
                    additionalReading();
                }
            }
        });
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Intent intent = new Intent(getActivity(), ProfileActivity.class);
        intent.putExtra(ProfileActivity.EXTRA_USER_ID, mAdapter.getItem(position).getId());
        startActivity(intent);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle args = new Bundle();
        args.putLong(ProfileActivity.EXTRA_USER_ID, mUserId);
        getLoaderManager().initLoader(0, args, this).forceLoad();
    }

    private void additionalReading() {
        if (!mAutoLoader) {
            return;
        }
        mAutoLoader = false;
        Bundle args = new Bundle();
        args.putLong(ProfileActivity.EXTRA_USER_ID, mUserId);
        args.putLong("cursor", mCursor);
        getLoaderManager().restartLoader(0, args, this).forceLoad();
    }

    @Override
    public Loader<PagableResponseList<User>> onCreateLoader(int id, Bundle args) {
        return new FollowersListLoader(getActivity(), args.getLong(ProfileActivity.EXTRA_USER_ID), args.getLong("cursor", -1));
    }

    @Override
    public void onLoadFinished(Loader<PagableResponseList<User>> loader, PagableResponseList<User> friendsList) {
        setListShown(true);

        if (friendsList == null) {
            mListView.removeFooterView(mFooter);
            return;
        }
        mAdapter.addAll(friendsList);
        mAdapter.notifyDataSetChanged();

        mCursor = friendsList.getNextCursor();
        if (friendsList.hasNext()) {
            mAutoLoader = true;
        }
    }

    @Override
    public void onLoaderReset(Loader<PagableResponseList<User>> loader) {

    }
}