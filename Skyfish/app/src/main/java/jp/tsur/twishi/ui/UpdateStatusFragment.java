package jp.tsur.twishi.ui;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import jp.tsur.twishi.MyTextWatcher;
import jp.tsur.twishi.R;
import jp.tsur.twishi.model.PostStock;
import jp.tsur.twishi.plugin.TwiccaPlugin;
import jp.tsur.twishi.task.UpdateStatusTask;
import jp.tsur.twishi.util.FileUtil;
import jp.tsur.twishi.util.MessageUtil;
import jp.tsur.twishi.util.ThemeUtil;
import jp.tsur.twishi.util.TwitterUtil;
import jp.tsur.twishi.widget.FontelloButton;
import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.TwitterException;


public class UpdateStatusFragment extends DialogFragment {

    private static final int REQUEST_GALLERY = 1;
    private static final int REQUEST_CAMERA = 2;
    private static final int REQUEST_TWICCA = 3;
    private static final int OPTION_MENU_GROUP_TWICCA = 1;
    public static final String EXTRA_STATUS_TEXT = "status_text";
    public static final String EXTRA_SELECTION = "selection";
    public static final String EXTRA_SELECTION_END = "selection_end";
    public static final String EXTRA_IN_REPLY_TO_STATUS = "in_reply_to_status";

    @InjectView(R.id.status_text)
    EditText mStatusText;
    @InjectView(R.id.img_button)
    FontelloButton mImgButton;
    @InjectView(R.id.draft_button)
    FontelloButton mDraftButton;
    @InjectView(R.id.hashtag_button)
    FontelloButton mHashtagButton;
    @InjectView(R.id.count)
    TextView mCount;
    @InjectView(R.id.in_reply_to_status)
    TextView mInReplyToStatus;
    @InjectView(R.id.tweet_button)
    FontelloButton mTweetButton;
    @InjectView(R.id.other_button)
    ImageView mOtherButton;

    private Context mContext;
    private AlertDialog mHashtagDialog;
    private AlertDialog mDraftDialog;
    private PostStock mPostStock;
    private List<ResolveInfo> mTwiccaPlugins;
    private File mImgPath;
    private Uri mImageUri;
    private long mInReplyToStatusId;

    public static UpdateStatusFragment newInstance(Serializable inReplayToStatus, String statusText, int selection, int selectionEnd) {
        UpdateStatusFragment f = new UpdateStatusFragment();
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_IN_REPLY_TO_STATUS, inReplayToStatus);
        args.putString(EXTRA_STATUS_TEXT, statusText);
        args.putInt(EXTRA_SELECTION, selection);
        args.putInt(EXTRA_SELECTION_END, selectionEnd);

        f.setArguments(args);
        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), R.style.NoTitleDialog);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        return dialog;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Dialog dialog = getDialog();

        assert dialog != null;
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        lp.width = (int) (metrics.widthPixels * 0.9);
        dialog.getWindow().setAttributes(lp);
        dialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_update_status, container);
        ButterKnife.inject(this, view);
        mContext = getActivity();

        Bundle args = getArguments();
        if (args != null) {

//        if (args.getBooleanExtra("notification", false)) {
//            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//            manager.cancelAll();
//        }
//        mWidgetMode = intent.getBooleanExtra("widget", false);
//        if (mWidgetMode) {
//            mActionBarHolder.mTitle.setText(getString(R.string.widget_title_post_mode));
//        } else {
//            mActionBarHolder.mTitle.setText(getString(R.string.title_post));
//        }

            //
            String status = args.getString(EXTRA_STATUS_TEXT);
            if (status != null) {
                mStatusText.setText(status);
            }

            int selectStart = args.getInt(EXTRA_SELECTION, 0);
            if (selectStart > 0) {
                int selectStop = args.getInt(EXTRA_SELECTION_END, 0);
                if (selectStop > 0) {
                    mStatusText.setSelection(selectStart, selectStop);
                } else {
                    mStatusText.setSelection(selectStart);
                }
            }

            Status inReplyToStatus = (Status) args.getSerializable(EXTRA_IN_REPLY_TO_STATUS);
            if (inReplyToStatus != null) {
                if (inReplyToStatus.getRetweetedStatus() != null) {
                    inReplyToStatus = inReplyToStatus.getRetweetedStatus();
                }
                mInReplyToStatusId = inReplyToStatus.getId();
                mInReplyToStatus.setText(inReplyToStatus.getText());
            }
        }
//        if (intent.getData() != null) {
//            String inReplyToStatusId = intent.getData().getQueryParameter("in_reply_to");
//            if (inReplyToStatusId != null && inReplyToStatusId.length() > 0) {
//                mInReplyToStatusId = Long.valueOf(inReplyToStatusId);
//            }
//
//            String text = intent.getData().getQueryParameter("text");
//            String url = intent.getData().getQueryParameter("url");
//            String hashtags = intent.getData().getQueryParameter("hashtags");
//            if (text == null) {
//                text = "";
//            }
//            if (url != null) {
//                text += " " + url;
//            }
//            if (hashtags != null) {
//                text += " #" + hashtags;
//            }
//            mStatusText.setText(text);
//        }

//        // ブラウザから来たとき
//        if (Intent.ACTION_SEND.equals(intent.getAction())) {
//            if (intent.getParcelableExtra(Intent.EXTRA_STREAM) != null) {
//                Uri imgUri = intent.getParcelableExtra(Intent.EXTRA_STREAM);
//                setImage(imgUri);
//            } else if (intent.getExtras() != null) {
//                String pageUri = intent.getExtras().getString(Intent.EXTRA_TEXT);
//                String pageTitle = intent.getExtras().getString(Intent.EXTRA_SUBJECT);
//                if (pageTitle == null) {
//                    pageTitle = "";
//                }
//                if (pageUri != null) {
//                    pageTitle += " " + pageUri;
//                }
//                mStatusText.setText(pageTitle);
//            }
//        }
        // 文字をカウントする
        if (mStatusText.getText() != null) {
            updateCount(mStatusText.getText().toString());
        }

        // 下書きとハッシュタグがあるかチェック
        mPostStock = new PostStock();
        if (mPostStock.getDrafts().isEmpty()) {
            mDraftButton.setEnabled(false);
        }
        if (mPostStock.getHashtags().isEmpty()) {
            mHashtagButton.setEnabled(false);
        }

        // 文字をカウントする
        if (mStatusText.getText() != null) {
            updateCount(mStatusText.getText().toString());
        }

        // 文字数をカウントしてボタンを制御する
        mStatusText.addTextChangedListener(new MyTextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                updateCount(s.toString());
            }
        });

        registerForContextMenu(mImgButton);

        // 下書きを保存するかどうか
        assert view != null;
        mStatusText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != KeyEvent.ACTION_DOWN) {
                    return false;
                }
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (!TextUtils.isEmpty(mStatusText.getText())) {
                        new AlertDialog.Builder(mContext)
                                .setMessage(R.string.confirm_save_draft)
                                .setPositiveButton(
                                        R.string.button_save,
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                mPostStock.addDraft(mStatusText.getText().toString());
                                                dismiss();
                                            }
                                        }
                                )
                                .setNegativeButton(
                                        R.string.button_destroy,
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dismiss();
                                            }
                                        }
                                )
                                .show();
                        return true;
                    }
                }
                return false;
            }
        });

        return view;
    }

    @OnClick(R.id.draft_button)
    void showDraft() {
        ListView listView = new ListView(mContext);
        mPostStock = new PostStock();
        final DraftAdapter adapter = new DraftAdapter(mContext, R.layout.list_item_word_with_trash, mPostStock.getDrafts());
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String draft = adapter.getItem(i);
                mStatusText.setText(draft);
                mDraftDialog.dismiss();
                adapter.remove(draft);
            }
        });

        mDraftDialog = new AlertDialog.Builder(mContext)
                .setTitle(R.string.dialog_title_draft)
                .setView(listView)
                .show();
    }

    @OnClick(R.id.hashtag_button)
    void showHashtag() {
        ListView listView = new ListView(mContext);
        mPostStock = new PostStock();
        final HashtagAdapter adapter = new HashtagAdapter(
                mContext, R.layout.list_item_word_with_trash, mPostStock.getHashtags());
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String hashtag = adapter.getItem(i);
                if (mStatusText.getText() != null) {
                    mStatusText.setText(mStatusText.getText().toString().concat(" ".concat(hashtag)));
                    mHashtagDialog.dismiss();
                }
            }
        });

        mHashtagDialog = new AlertDialog.Builder(mContext)
                .setTitle(R.string.dialog_title_hashtag)
                .setView(listView)
                .show();
    }

    @OnClick(R.id.img_button)
    void setImage() {
        mImgButton.showContextMenu();
    }

    @OnClick(R.id.other_button)
    void openMenu() {
        PopupMenu popup = new PopupMenu(mContext, mOtherButton);
        popup.getMenuInflater().inflate(R.menu.update_status, popup.getMenu());
        // twiccaプラグインを読む
        if (mTwiccaPlugins == null) {
            mTwiccaPlugins = TwiccaPlugin.getResolveInfo(mContext.getPackageManager(),
                    TwiccaPlugin.TWICCA_ACTION_EDIT_TWEET);
        }
        if (!mTwiccaPlugins.isEmpty()) {
            PackageManager pm = mContext.getPackageManager();
            int i = 0;
            for (ResolveInfo resolveInfo : mTwiccaPlugins) {
                if (pm == null || resolveInfo.activityInfo == null) {
                    continue;
                }
                String label = (String) resolveInfo.activityInfo.loadLabel(pm);
                if (label == null) {
                    continue;
                }
                popup.getMenu().add(OPTION_MENU_GROUP_TWICCA, i, 100, label);
                i++;
            }
        }
        popup.show();
        // ポップアップメニューのメニュー項目のクリック処理
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getGroupId() == OPTION_MENU_GROUP_TWICCA) {
                    ResolveInfo resolveInfo = mTwiccaPlugins.get(item.getItemId());
                    if (resolveInfo.activityInfo != null && mStatusText.getText() != null) {
                        Intent intent = TwiccaPlugin.createIntentEditTweet(
                                "", mStatusText.getText().toString(), "", 0, resolveInfo.activityInfo.packageName,
                                resolveInfo.activityInfo.name);
                        startActivityForResult(intent, REQUEST_TWICCA);
                    }
                    return true;
                }
                switch (item.getItemId()) {
                    case R.id.tweet_clear:
                        mStatusText.setText("");
                        break;
                    case R.id.tweet_battery:
                        mStatusText.setText(TwitterUtil.getBatteryStatus(mContext));
                        break;
                    case R.id.convert_suddenly:
                        String text = mStatusText.getText().toString();
                        int selectStart = mStatusText.getSelectionStart();
                        int selectEnd = mStatusText.getSelectionEnd();
                        mStatusText.setText(TwitterUtil.convertSuddenly(text, selectStart, selectEnd));
                        break;
                }
                return true;
            }
        });
    }

    @OnClick(R.id.tweet_button)
    void tweet() {
        assert mStatusText.getText() != null;
        String text = mStatusText.getText().toString();

        StatusUpdate statusUpdate = new StatusUpdate(text);
        if (mImgPath != null) {
            statusUpdate.setMedia(mImgPath);
        }
        if (mInReplyToStatusId > 0) {
            statusUpdate.setInReplyToStatusId(mInReplyToStatusId);
        }

        UpdateStatusTask task = new UpdateStatusTask() {
            @Override
            protected void onPostExecute(TwitterException e) {
                if (e == null) {
                    mStatusText.setText("");
                    dismiss();
                } else if (e.getErrorCode() == TwitterUtil.ERROR_CODE_DUPLICATE_STATUS) {
                    MessageUtil.showToast(getString(R.string.toast_update_status_already));
                } else {
                    MessageUtil.showToast(R.string.toast_update_status_failure);
                }
            }
        };
        task.execute(statusUpdate);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.setHeaderTitle(getString(R.string.context_menu_title_photo_method));
        menu.add(0, REQUEST_GALLERY, 0, R.string.context_menu_photo_gallery);
        menu.add(0, REQUEST_CAMERA, 0, R.string.context_menu_photo_camera);

        MenuItem.OnMenuItemClickListener listener = new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                onContextItemSelected(item);
                return true;
            }
        };

        for (int i = 0, n = menu.size(); i < n; i++)
            menu.getItem(i).setOnMenuItemClickListener(listener);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case REQUEST_GALLERY:
                intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, REQUEST_GALLERY);
                return true;
            case REQUEST_CAMERA:
                String filename = System.currentTimeMillis() + ".jpg";
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.TITLE, filename);
                values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
                mImageUri = mContext.getContentResolver().insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

                intent = new Intent();
                intent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
                startActivityForResult(intent, REQUEST_CAMERA);
                return true;
            default:
                return true;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case REQUEST_GALLERY:
                setImage(data.getData());
                break;
            case REQUEST_CAMERA:
                setImage(mImageUri);
                break;
            case REQUEST_TWICCA:
                mStatusText.setText(data.getStringExtra(Intent.EXTRA_TEXT));
                break;
        }
    }

    private void setImage(Uri uri) {
        try {
            InputStream inputStream = mContext.getContentResolver().openInputStream(uri);
            mImgPath = FileUtil.writeToTempFile(mContext.getCacheDir(), inputStream);
            mImgButton.setTextColor(Color.BLUE);
            mTweetButton.setEnabled(true);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void updateCount(String str) {
        int textColor;

        int length = TwitterUtil.count(str);
        // 140文字をオーバーした時は文字数を赤色に
        if (length < 0) {
            textColor = Color.RED;
        } else {
            textColor = ThemeUtil.getThemeTextColor(R.attr.menu_text_color);
        }
        mCount.setTextColor(textColor);
        mCount.setText(String.valueOf(length));

        if (length < 0 || length == 140) {
            // 文字数が0文字または140文字以上の時はボタンを無効
            if (mImgPath != null) {
                mTweetButton.setEnabled(true);
            } else {
                mTweetButton.setEnabled(false);
            }
        } else {
            mTweetButton.setEnabled(true);
        }
    }

    /**
     * 下書きアダプター
     */
    public class DraftAdapter extends ArrayAdapter<String> {

        private LayoutInflater mInflater;
        private int mLayout;

        public DraftAdapter(Context context, int resource, List<String> draftList) {
            super(context, resource, draftList);
            mInflater = LayoutInflater.from(context);
            mLayout = resource;
        }

        @Override
        public void remove(String draft) {
            super.remove(draft);
            mPostStock.removeDraft(draft);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View view = convertView;
            if (view == null) {
                view = mInflater.inflate(this.mLayout, null);
            }

            final String draft = getItem(position);

            assert view != null;
            ((TextView) view.findViewById(R.id.word)).setText(draft);

            view.findViewById(R.id.trash).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    remove(draft);
                }
            });
            return view;
        }
    }

    /**
     * ハッシュタグアダプター
     */
    public class HashtagAdapter extends ArrayAdapter<String> {

        private LayoutInflater mInflater;
        private int mLayout;

        public HashtagAdapter(Context context, int resource, List<String> hashtagList) {
            super(context, resource, hashtagList);
            mInflater = LayoutInflater.from(context);
            mLayout = resource;
        }

        @Override
        public void remove(String hashtag) {
            super.remove(hashtag);
            mPostStock.removeHashtag(hashtag);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View view = convertView;
            if (view == null) {
                view = mInflater.inflate(this.mLayout, null);
            }

            final String hashtag = getItem(position);

            assert view != null;
            ((TextView) view.findViewById(R.id.word)).setText(hashtag);

            view.findViewById(R.id.trash).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    remove(hashtag);
                }
            });
            return view;
        }
    }
}