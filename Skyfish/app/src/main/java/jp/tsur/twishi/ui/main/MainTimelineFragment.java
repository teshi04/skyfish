package jp.tsur.twishi.ui.main;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import jp.tsur.twishi.AccessTokenManager;
import jp.tsur.twishi.TabManager;
import jp.tsur.twishi.model.Row;
import jp.tsur.twishi.task.HomeTimelineLoader;
import twitter4j.ResponseList;
import twitter4j.Status;


public class MainTimelineFragment extends BaseMainTlFragment
        implements LoaderManager.LoaderCallbacks<ResponseList<Status>> {

    /**
     * このタブを表す固有のID、ユーザーリストで正数を使うため負数を使う
     */
    public long getTabId() {
        return TabManager.TIMELINE_TAB_ID;
    }

    /**
     * このタブに表示するツイートの定義
     *
     * @param row ストリーミングAPIから受け取った情報（ツイート）
     * @return trueは表示しない、falseは表示する
     */
    @Override
    protected boolean skip(Row row) {
        if (row.isStatus()) {
            Status retweet = row.getStatus().getRetweetedStatus();
            return retweet != null && retweet.getUser().getId() == AccessTokenManager.getUserId();
        } else {
            return true;
        }
    }

    @Override
    protected void initLoader() {
        Bundle args = new Bundle();
        getLoaderManager().restartLoader(0, args, this).forceLoad();
    }

    @Override
    protected void restartLoader() {
        Bundle args = new Bundle();
        if (!mReload) {
            args.putLong("max_id", mMaxId);
        }
        getLoaderManager().restartLoader((int) getTabId(), args, this).forceLoad();
    }

    @Override
    public Loader<ResponseList<Status>> onCreateLoader(int id, Bundle args) {
        return new HomeTimelineLoader(getActivity(), args.getLong("max_id"));
    }

    @Override
    public void onLoadFinished(Loader<ResponseList<Status>> loader, ResponseList<Status> statuses) {
        setListShown(true);
        if (statuses == null || statuses.size() == 0) {
            mReload = false;
            mPullToRefreshLayout.setRefreshComplete();
            return;
        }
        if (mReload) {
            clear();
            for (twitter4j.Status status : statuses) {
                mAdapter.add(Row.newStatus(status));
            }
            mReload = false;
            mPullToRefreshLayout.setRefreshComplete();
        } else {
            for (twitter4j.Status status : statuses) {
                mAdapter.extensionAdd(Row.newStatus(status));
            }
            mAutoLoader = true;
        }

        mMaxId = statuses.get(statuses.size() - 1).getId();
    }

    @Override
    public void onLoaderReset(Loader<ResponseList<Status>> loader) {

    }
}
