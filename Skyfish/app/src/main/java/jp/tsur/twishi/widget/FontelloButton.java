package jp.tsur.twishi.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

import jp.tsur.twishi.MyApplication;


public class FontelloButton extends Button {

    public FontelloButton(Context context) {
        super(context);
        init();
    }

    public FontelloButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (isInEditMode()) {
            return;
        }
        init();
    }

    private void init() {
        setTypeface(MyApplication.getApplication().getFontello());
    }
}