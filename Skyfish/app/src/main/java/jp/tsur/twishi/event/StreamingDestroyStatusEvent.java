package jp.tsur.twishi.event;

public class StreamingDestroyStatusEvent {

    private final long mStatusId;

    public StreamingDestroyStatusEvent(final long statusId) {
        mStatusId = statusId;
    }

    public long getStatusId() {
        return mStatusId;
    }
}
