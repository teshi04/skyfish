package jp.tsur.twishi.event;


import jp.tsur.twishi.model.Row;

public class NotificationEvent {

    private final Row mRow;

    public NotificationEvent(final Row row) {
        mRow = row;
    }

    public Row getRow() {
        return mRow;
    }
}
