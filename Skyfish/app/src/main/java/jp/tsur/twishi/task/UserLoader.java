package jp.tsur.twishi.task;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.text.TextUtils;

import jp.tsur.twishi.AccessTokenManager;
import jp.tsur.twishi.R;
import jp.tsur.twishi.TwitterManager;
import jp.tsur.twishi.model.Profile;
import jp.tsur.twishi.model.ProfileResponse;
import twitter4j.Paging;
import twitter4j.Relationship;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.User;

/**
 * スクリーンネーム、IDからユーザ情報を取得
 */
public class UserLoader extends AsyncTaskLoader<ProfileResponse> {

    private String mScreenName;
    private long mUserId;

    public UserLoader(Context context, String screenName) {
        super(context);
        mScreenName = screenName;
    }

    public UserLoader(Context context, long userId) {
        super(context);
        mUserId = userId;
    }

    @Override
    public ProfileResponse loadInBackground() {
        try {
            Twitter twitter = TwitterManager.getTwitter();
            User user;

            if (!TextUtils.isEmpty(mScreenName)) {
                user = twitter.showUser(mScreenName);
            } else {
                user = twitter.showUser(mUserId);
            }

            Relationship relationship = twitter.showFriendship(AccessTokenManager.getUserId(), user.getId());

            Paging paging = new Paging();
            paging.setCount(30);

            ResponseList<Status> userTimeline = TwitterManager.getTwitter().getUserTimeline(user.getId(), paging);

            Profile profile = new Profile(user, relationship, userTimeline);

            return new ProfileResponse(profile);
        } catch (TwitterException e) {
            e.printStackTrace();
            if (e.getStatusCode() == -1) {
                return new ProfileResponse(getContext().getString(R.string.toast_connection_failure));
            }
            if (e.getErrorCode() == 34) {
                return new ProfileResponse(getContext().getString(R.string.toast_user_not_found));
            }

            return null;
        }
    }
}