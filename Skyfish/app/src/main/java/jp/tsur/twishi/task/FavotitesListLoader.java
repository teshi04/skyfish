package jp.tsur.twishi.task;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import jp.tsur.twishi.TwitterManager;
import twitter4j.Paging;
import twitter4j.ResponseList;
import twitter4j.Status;


/**
 * お気に入りを取得
 */
public class FavotitesListLoader extends AsyncTaskLoader<ResponseList<Status>> {

    private long mUserId;
    private long mMaxId;

    public FavotitesListLoader(Context context, long userId, long maxId) {
        super(context);
        mUserId = userId;
        mMaxId = maxId;
    }

    @Override
    public ResponseList<Status> loadInBackground() {
        try {
            Paging paging = new Paging();
            if (mMaxId > 0) {
                paging.setMaxId(mMaxId - 1);
                paging.setCount(100);//BasicSettings.getPageCount());
            }
            return TwitterManager.getTwitter().getFavorites(mUserId, paging);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}