package jp.tsur.twishi.event;


import jp.tsur.twishi.model.Row;

public class StreamingCreateStatusEvent {
    private final Row mRow;

    public StreamingCreateStatusEvent(Row row) {
        mRow = row;
    }

    public Row getRow() {
        return mRow;
    }
}
