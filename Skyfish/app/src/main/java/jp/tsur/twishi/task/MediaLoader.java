package jp.tsur.twishi.task;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import jp.tsur.twishi.TwitterManager;
import twitter4j.MediaEntity;
import twitter4j.Status;
import twitter4j.TwitterException;

/**
 * ステータス ID から画像の URL を取得
 */
public class MediaLoader extends AsyncTaskLoader<String> {

    private long mStatusId;
    private int mIndex;

    public MediaLoader(Context context, long statusId, int index) {
        super(context);
        mStatusId = statusId;
        mIndex = index;
    }

    @Override
    public String loadInBackground() {
        try {
            Status status = TwitterManager.getTwitter().showStatus(mStatusId);
            MediaEntity[] mediaEntities = status.getMediaEntities();
            if (mediaEntities.length < mIndex) {
                return null;
            }
            return mediaEntities[mIndex - 1].getMediaURL();
        } catch (TwitterException e) {
            e.printStackTrace();
            return null;
        }
    }
}