package jp.tsur.twishi.task;

import android.os.AsyncTask;

import jp.tsur.twishi.TwitterManager;
import twitter4j.ResponseList;
import twitter4j.SavedSearch;


/**
 * 保存した検索を取得
 */
public class SavedSearchesTask extends AsyncTask<Void, Void, ResponseList<SavedSearch>> {

    @Override
    protected ResponseList<SavedSearch> doInBackground(Void... params) {
        try {
            return TwitterManager.getTwitter().getSavedSearches();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}