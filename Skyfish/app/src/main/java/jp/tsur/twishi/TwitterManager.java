package jp.tsur.twishi;


import android.os.AsyncTask;

import de.greenrobot.event.EventBus;
import jp.tsur.twishi.adapter.MyUserStreamAdapter;
import jp.tsur.twishi.event.StreamingConnectionEvent;
import jp.tsur.twishi.model.BasicSettings;
import twitter4j.ConnectionLifeCycleListener;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.auth.AccessToken;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Twitterインスタンス管理
 */
public class TwitterManager {
    private static MyUserStreamAdapter sUserStreamAdapter;
    private static TwitterStream sTwitterStream;
    private static Twitter mTwitter;
    private static boolean sTwitterStreamConnected;

    private static String getConsumerKey() {
        return MyApplication.getApplication().getString(R.string.twitter_consumer_key);
    }

    private static String getConsumerSecret() {
        return MyApplication.getApplication().getString(R.string.twitter_consumer_secret);
    }

    public static Twitter getTwitter() {
        if (mTwitter != null) {
            return mTwitter;
        }
        Twitter twitter = getTwitterInstance();

        AccessToken token = AccessTokenManager.getAccessToken();
        if (token != null) {
            twitter.setOAuthAccessToken(token);
            // アクセストークンまである時だけキャッシュしておく
            mTwitter = twitter;
        }
        return twitter;
    }

    public static Twitter getTwitterInstance() {

        TwitterFactory factory = new TwitterFactory();
        Twitter twitter = factory.getInstance();
        twitter.setOAuthConsumer(getConsumerKey(), getConsumerSecret());

        return twitter;
    }

    public static TwitterStream getTwitterStream() {
        AccessToken token = AccessTokenManager.getAccessToken();
        if (token == null) {
            return null;
        }
        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
        twitter4j.conf.Configuration conf = configurationBuilder.setOAuthConsumerKey(getConsumerKey())
                .setOAuthConsumerSecret(getConsumerSecret()).setOAuthAccessToken(token.getToken())
                .setOAuthAccessTokenSecret(token.getTokenSecret()).build();
        return new TwitterStreamFactory(conf).getInstance();
    }

    public static boolean getTwitterStreamConnected() {
        return sTwitterStreamConnected;
    }

    public static void startStreaming() {
        if (sTwitterStream != null) {
            if (!sTwitterStreamConnected) {
                sUserStreamAdapter.start();
                sTwitterStream.setOAuthAccessToken(AccessTokenManager.getAccessToken());
                sTwitterStream.user();
            }
            return;
        }
        sTwitterStream = getTwitterStream();
        sUserStreamAdapter = new MyUserStreamAdapter();
        sTwitterStream.addListener(sUserStreamAdapter);
        sTwitterStream.addConnectionLifeCycleListener(new MyConnectionLifeCycleListener());
        sTwitterStream.user();
        BasicSettings.setStreamingMode(true);
//        BasicSettings.resetNotification();
    }

    public static void stopStreaming() {
        if (sTwitterStream == null) {
            return;
        }
        BasicSettings.setStreamingMode(false);
        sUserStreamAdapter.stop();
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                sTwitterStream.cleanUp();
                sTwitterStream.shutdown();
                return null;
            }

            @Override
            protected void onPostExecute(Void status) {

            }
        }.execute();
    }

    public static void pauseStreaming() {
        if (sUserStreamAdapter != null) {
            sUserStreamAdapter.pause();
        }
    }

    public static void resumeStreaming() {
        if (sUserStreamAdapter != null) {
            sUserStreamAdapter.resume();
        }
    }

    public static class MyConnectionLifeCycleListener implements ConnectionLifeCycleListener {
        @Override
        public void onConnect() {
            sTwitterStreamConnected = true;
            EventBus.getDefault().post(StreamingConnectionEvent.onConnect());
        }

        @Override
        public void onDisconnect() {
            sTwitterStreamConnected = false;
            EventBus.getDefault().post(StreamingConnectionEvent.onDisconnect());
        }

        @Override
        public void onCleanUp() {
            sTwitterStreamConnected = false;
            EventBus.getDefault().post(StreamingConnectionEvent.onCleanUp());
        }
    }
}
