package jp.tsur.twishi.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;

import jp.tsur.twishi.R;
import jp.tsur.twishi.task.SavedSearchesTask;
import twitter4j.ResponseList;
import twitter4j.SavedSearch;

public class SearchAdapter extends ArrayAdapter<String> implements Filterable {

    private ArrayList<String> mStrings = new ArrayList<>();
    private ArrayList<String> mSavedSearches = new ArrayList<>();
    private String mSearchWord;
    private LayoutInflater mInflater;
    private int mLayout;
    private Context mContext;
    private boolean mSavedMode = false;

    public SearchAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        mContext = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mLayout = textViewResourceId;

        new SavedSearchesTask() {
            @Override
            protected void onPostExecute(ResponseList<SavedSearch> savedSearches) {
                if (savedSearches == null) {
                    return;
                }
                for (SavedSearch savedSearch : savedSearches) {
                    mSavedSearches.add(savedSearch.getQuery());
                }
            }
        }.execute();
    }

    public boolean isSavedMode() {
        return mSavedMode;
    }

    @Override
    public String getItem(int position) {
        return mStrings.get(position);
    }

    @Override
    public int getCount() {
        return mStrings.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                mSavedMode = constraint == null || constraint.length() == 0;
                if (mSavedMode) {
                    mStrings = mSavedSearches;
                } else {
                    mSearchWord = constraint.toString();
                    mStrings = new ArrayList<>();
                    mStrings.add(mSearchWord + mContext.getString(R.string.label_search_tweet));
                    mStrings.add(mSearchWord + mContext.getString(R.string.label_search_user));
                    mStrings.add("@" + mSearchWord + mContext.getString(R.string.label_display_profile));
                }
                filterResults.values = mStrings;
                filterResults.count = mStrings.size();
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }

            @Override
            public String convertResultToString(Object resultValue) {
                //ここでフィルタリングした値を選択したときに返す値を実装
                if (mSavedMode) {
                    return (String) resultValue;
                } else {
                    return mSearchWord;
                }
            }
        };
    }
}
