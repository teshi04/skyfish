package jp.tsur.twishi.model;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.util.ArrayList;

import jp.tsur.twishi.MyApplication;

public class PostStock {

    private Application mApplication;
    private PostStockDate mPostStockSettingsDate;
    private static final String PREF_NAME = "post_stock";
    private static final String PREF_KEY = "data";

    public PostStock() {
        mApplication = MyApplication.getApplication();
        loadPostStock();
    }

    public void loadPostStock() {
        SharedPreferences preferences = mApplication.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        String json = preferences.getString(PREF_KEY, null);
        Gson gson = new Gson();
        if (json != null) {
            mPostStockSettingsDate = gson.fromJson(json, PostStockDate.class);
        } else {
            mPostStockSettingsDate = new PostStockDate();
            mPostStockSettingsDate.hashtags = new ArrayList<String>();
            mPostStockSettingsDate.drafts = new ArrayList<String>();

        }
    }

    public void savePostStockSettings() {
        SharedPreferences preferences = mApplication.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String exportJson = gson.toJson(mPostStockSettingsDate);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PREF_KEY, exportJson);
        editor.commit();
    }

    public void addHashtag(String hashtag) {
        for (String existHashtag : mPostStockSettingsDate.hashtags) {
            if (existHashtag.equals(hashtag)) {
                return;
            }
        }
        mPostStockSettingsDate.hashtags.add(0, hashtag);
        savePostStockSettings();
    }

    public void removeHashtag(String hashtag) {
        mPostStockSettingsDate.hashtags.remove(hashtag);
        savePostStockSettings();
    }

    public void addDraft(String draft) {
        for (String muteWord : mPostStockSettingsDate.hashtags) {
            if (muteWord.equals(draft)) {
                return;
            }
        }
        mPostStockSettingsDate.drafts.add(0, draft);
        savePostStockSettings();
    }

    public void removeDraft(String draft) {
        mPostStockSettingsDate.drafts.remove(draft);
        savePostStockSettings();
    }

    public ArrayList<String> getHashtags() {
        return mPostStockSettingsDate.hashtags;
    }

    public ArrayList<String> getDrafts() {
        return mPostStockSettingsDate.drafts;
    }

    public static class PostStockDate {
        ArrayList<String> hashtags;
        ArrayList<String> drafts;
    }
}
