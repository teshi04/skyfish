package jp.tsur.twishi.model;


public class ProfileResponse {

    public ProfileResponse(Profile profile) {
        mErrorMessage = null;
        mProfile = profile;
    }

    public ProfileResponse(String errorMessage) {
        if (errorMessage == null) {
            throw new NullPointerException("errorMessage is null.");
        }

        mErrorMessage = errorMessage;
        mProfile = null;
    }

    private final Profile mProfile;
    private final String mErrorMessage;

    public interface Visitor {
        public void onSuccess(Profile profile);

        public void onFailure(String errorMessage);
    }

    public void visit(Visitor visitor) {
        if (mErrorMessage != null) {
            visitor.onFailure(mErrorMessage);
        } else {
            visitor.onSuccess(mProfile);
        }
    }
}
