package jp.tsur.twishi.util;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import jp.tsur.twishi.MyApplication;

public class KeyboardUtil {

    public static void showKeyboard(final View view) {
        showKeyboard(view, 200);
    }

    public static void showKeyboard(final View view, int delay) {
        view.postDelayed(new Runnable() {
            @Override
            public void run() {
                // キーボードは EditText にフォーカスが当たっているときでないと出てこない
                final View.OnFocusChangeListener listener = view.getOnFocusChangeListener();
                view.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean has_focus) {
                        if (!has_focus) {
                            return;
                        }
                        getInputMethodManager().showSoftInput(v, InputMethodManager.SHOW_FORCED);
                        v.setOnFocusChangeListener(listener);
                    }
                });
                view.clearFocus();
                view.requestFocus();
            }
        }, delay);
    }

    public static void hideKeyboard(View view) {
        getInputMethodManager().hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static InputMethodManager getInputMethodManager() {
        return (InputMethodManager) MyApplication.getApplication()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
    }
}
