package jp.tsur.twishi.ui.settings;

import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;

import jp.tsur.twishi.R;

public class PerformanceSettingsFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_performance);

        ListPreference userIconSizePreference = (ListPreference) findPreference("user_icon_size");
        if (userIconSizePreference != null) {
            userIconSizePreference.setSummary(userIconSizePreference.getEntry());
            userIconSizePreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    ListPreference listPreference = (ListPreference) preference;
                    int listId = listPreference.findIndexOfValue((String) newValue);
                    CharSequence[] entries;
                    entries = listPreference.getEntries();
                    if (entries != null) {
                        preference.setSummary(entries[listId]);
                    }
                    return true;
                }
            });
        }

        ListPreference pageCountPreference = (ListPreference) findPreference("page_count");
        if (pageCountPreference != null) {
            pageCountPreference.setSummary(pageCountPreference.getEntry());
            pageCountPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    ListPreference listPreference = (ListPreference) preference;
                    int listId = listPreference.findIndexOfValue((String) newValue);
                    CharSequence[] entries;
                    entries = listPreference.getEntries();
                    if (entries != null) {
                        preference.setSummary(entries[listId]);
                    }
                    return true;
                }
            });
        }
    }

}
