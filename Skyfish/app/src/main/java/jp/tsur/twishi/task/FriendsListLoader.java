package jp.tsur.twishi.task;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import jp.tsur.twishi.TwitterManager;
import twitter4j.PagableResponseList;
import twitter4j.User;


/**
 * フォロー取得
 */
public class FriendsListLoader extends AsyncTaskLoader<PagableResponseList<User>> {

    private long mUserId;
    private long mCursor;

    public FriendsListLoader(Context context, long userId, long cursor) {
        super(context);
        mUserId = userId;
        mCursor = cursor;
    }

    @Override
    public PagableResponseList<User> loadInBackground() {
        try {
            return TwitterManager.getTwitter().getFriendsList(mUserId, mCursor);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
