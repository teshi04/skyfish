package jp.tsur.twishi.ui.status;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import jp.tsur.twishi.R;
import jp.tsur.twishi.adapter.UserAdapter;
import jp.tsur.twishi.task.RetweetLoader;
import jp.tsur.twishi.util.MessageUtil;
import twitter4j.ResponseList;
import twitter4j.Status;

/**
 * リツイートした人を表示
 */
public class RetweetUserFragment extends DialogFragment
        implements LoaderManager.LoaderCallbacks<ResponseList<Status>> {

    private ProgressBar mProgressBar;
    private UserAdapter mAdapter;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Activity activity = getActivity();
        Dialog dialog = new Dialog(activity, R.style.NoTitleDialog);
        dialog.setContentView(R.layout.fragment_retweet_user);

        ListView listView = (ListView) dialog.findViewById(R.id.list_view);
        mProgressBar = (ProgressBar) dialog.findViewById(R.id.guruguru);

        mAdapter = new UserAdapter(activity, R.layout.list_item_user);
        listView.setAdapter(mAdapter);

        return dialog;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(0, getArguments(), this).forceLoad();
    }

    @Override
    public Loader<ResponseList<Status>> onCreateLoader(int id, Bundle args) {
        return new RetweetLoader(getActivity(), args.getLong("status_id"));
    }

    @Override
    public void onLoadFinished(Loader<ResponseList<Status>> loader, ResponseList<Status> statuses) {
        mProgressBar.setVisibility(View.GONE);
        if (statuses != null) {
            for (twitter4j.Status status : statuses) {
                mAdapter.add(status.getUser());
            }
            mAdapter.notifyDataSetChanged();
        } else {
            MessageUtil.showToast(R.string.toast_load_data_failure);
        }
    }

    @Override
    public void onLoaderReset(Loader<ResponseList<Status>> loader) {

    }
}
