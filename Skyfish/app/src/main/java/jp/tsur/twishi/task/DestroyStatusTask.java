package jp.tsur.twishi.task;

import android.os.AsyncTask;

import jp.tsur.twishi.R;
import jp.tsur.twishi.TwitterManager;
import jp.tsur.twishi.util.MessageUtil;

// TODO:

/**
 * ステータスを削除
 */
public class DestroyStatusTask extends AsyncTask<Long, Void, Boolean> {

    private long mStatusId;

    public DestroyStatusTask(long statusId) {
        mStatusId = statusId;
    }

    @Override
    protected Boolean doInBackground(Long... params) {
        try {
            TwitterManager.getTwitter().destroyStatus(mStatusId);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    protected void onPostExecute(Boolean success) {
        if (success) {
            MessageUtil.showToast(R.string.toast_destroy_status_success);
//            EventBus.getDefault().post(new StreamingDestroyStatusEvent(mStatusId));
        } else {
            MessageUtil.showToast(R.string.toast_destroy_status_failure);
        }
    }
}