package jp.tsur.twishi.ui.main;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import jp.tsur.twishi.AccessTokenManager;
import jp.tsur.twishi.TabManager;
import jp.tsur.twishi.event.StreamingCreateFavoriteEvent;
import jp.tsur.twishi.model.Row;
import jp.tsur.twishi.task.MentionsTimelineLoader;
import jp.tsur.twishi.util.StatusUtil;
import twitter4j.ResponseList;
import twitter4j.Status;


public class InteractionsFragment extends BaseMainTlFragment
        implements LoaderManager.LoaderCallbacks<ResponseList<Status>> {

    /**
     * このタブを表す固有のID、ユーザーリストで正数を使うため負数を使う
     */
    public long getTabId() {
        return TabManager.INTERACTIONS_TAB_ID;
    }

    @Override
    protected void initLoader() {
        Bundle args = new Bundle();
        getLoaderManager().restartLoader(0, args, this).forceLoad();
    }

    @Override
    protected void restartLoader() {
        Bundle args = new Bundle();
        if (!mReload) {
            args.putLong("max_id", mMaxId);
        }
        getLoaderManager().restartLoader((int) getTabId(), args, this).forceLoad();
    }

    /**
     * このタブに表示するツイートの定義
     *
     * @param row ストリーミングAPIから受け取った情報（ツイート＋ふぁぼ）
     *            CreateFavoriteEventをキャッチしている為、ふぁぼイベントを受け取ることが出来る
     * @return trueは表示しない、falseは表示する
     */
    @Override
    protected boolean skip(Row row) {
        if (row.isFavorite()) {
            return false;
        }
        if (row.isStatus()) {

            Status status = row.getStatus();
            Status retweet = status.getRetweetedStatus();

            // 自分のツイートがRTされた時
            if (retweet != null && retweet.getUser().getId() == AccessTokenManager.getUserId()) {
                return false;
            }

            // 自分宛のメンション（但し「自分をメンションに含むツイートがRTされた時」は除く）
            if (retweet == null && StatusUtil.isMentionForMe(status)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Loader<ResponseList<Status>> onCreateLoader(int id, Bundle args) {
        return new MentionsTimelineLoader(getActivity(), args.getLong("max_id"));
    }

    @Override
    public void onLoadFinished(Loader<ResponseList<Status>> loader, ResponseList<Status> statuses) {
        setListShown(true);
        if (statuses == null || statuses.size() == 0) {
            mReload = false;
            mPullToRefreshLayout.setRefreshComplete();
            return;
        }
        if (mReload) {
            clear();
            for (twitter4j.Status status : statuses) {
                mAdapter.add(Row.newStatus(status));
            }
            mReload = false;
            mPullToRefreshLayout.setRefreshComplete();
        } else {
            for (twitter4j.Status status : statuses) {
                mAdapter.extensionAdd(Row.newStatus(status));
            }
            mAutoLoader = true;
        }

        mMaxId = statuses.get(statuses.size() - 1).getId();
    }

    @Override
    public void onLoaderReset(Loader<ResponseList<Status>> loader) {

    }

    public void onEventMainThread(StreamingCreateFavoriteEvent event) {
        addStack(event.getRow());
    }
}
