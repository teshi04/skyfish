package jp.tsur.twishi.ui.settings;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import jp.tsur.twishi.R;

public class NotificationSettingsFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_notification);
    }

}