package jp.tsur.twishi.task;

import android.os.AsyncTask;

import jp.tsur.twishi.TwitterManager;


/**
 * スパム報告
 */
public class ReportSpamTask extends AsyncTask<Long, Void, Boolean> {
    @Override
    protected Boolean doInBackground(Long... params) {
        Long userId = params[0];
        try {
            TwitterManager.getTwitter().reportSpam(userId);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}