package jp.tsur.twishi.ui.profile;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;

import jp.tsur.twishi.R;
import jp.tsur.twishi.adapter.UserListAdapter;
import jp.tsur.twishi.task.UserListMembershipsLoader;
import twitter4j.PagableResponseList;
import twitter4j.UserList;

/**
 * ユーザーが追加されているリスト
 */
public class UserListMembershipsFragment extends ListFragment
        implements LoaderManager.LoaderCallbacks<PagableResponseList<UserList>> {

    private UserListAdapter mAdapter;
    private long mUserId;
    private long mCursor;
    private ListView mListView;
    private View mFooter;
    private boolean mAutoLoader;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mListView = getListView();

        LayoutInflater inflater = LayoutInflater.from(getActivity());
        mFooter = inflater.inflate(R.layout.guruguru, mListView, false);
        mListView.addFooterView(mFooter, null, false);
        mAdapter = new UserListAdapter(getActivity(), R.layout.list_item_user);
        setListAdapter(mAdapter);
        setListShown(false);

        mUserId = getArguments().getLong(ProfileActivity.EXTRA_USER_ID);

        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (totalItemCount == firstVisibleItem + visibleItemCount) {
                    additionalReading();
                }
            }
        });
    }

    // TODO:
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
//                Intent intent = new Intent(v.getContext(), UserListActivity.class);
//                intent.putExtra("userList", userList);
//                startActivity(intent);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle args = new Bundle();
        args.putLong(ProfileActivity.EXTRA_USER_ID, mUserId);
        getLoaderManager().initLoader(0, args, this).forceLoad();
    }

    private void additionalReading() {
        if (!mAutoLoader) {
            return;
        }
        mAutoLoader = false;
        Bundle args = new Bundle();
        args.putLong(ProfileActivity.EXTRA_USER_ID, mUserId);
        args.putLong("cursor", mCursor);
        getLoaderManager().restartLoader(0, args, this).forceLoad();
    }

    @Override
    public Loader<PagableResponseList<UserList>> onCreateLoader(int id, Bundle args) {
        return new UserListMembershipsLoader(getActivity(), args.getLong(ProfileActivity.EXTRA_USER_ID), args.getLong("cursor", -1));
    }

    @Override
    public void onLoadFinished(Loader<PagableResponseList<UserList>> loader, PagableResponseList<UserList> userLists) {
        setListShown(true);

        if (userLists == null) {
            mListView.removeFooterView(mFooter);
            return;
        }
        mCursor = userLists.getNextCursor();
        for (UserList userlist : userLists) {
            mAdapter.add(userlist);
        }
        if (userLists.hasNext()) {
            mAutoLoader = true;
        }
    }

    @Override
    public void onLoaderReset(Loader<PagableResponseList<UserList>> loader) {

    }
}
