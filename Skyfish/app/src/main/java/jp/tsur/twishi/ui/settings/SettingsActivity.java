package jp.tsur.twishi.ui.settings;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.view.MenuItem;

import jp.tsur.twishi.R;
import jp.tsur.twishi.ui.LicenceActivity;


public class SettingsActivity extends PreferenceActivity {

    public static Intent getLaunchIntent(Context context) {
        Intent intent = new Intent(context, SettingsActivity.class);
        intent.putExtra(PreferenceActivity.EXTRA_SHOW_FRAGMENT, SettingsFragment.class.getName());
        intent.putExtra(PreferenceActivity.EXTRA_NO_HEADERS, true);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

    }

    @Override
    protected boolean isValidFragment(String fragmentName) {
        // 使用できる Fragment か確認する
        return NotificationSettingsFragment.class.getName().equals(fragmentName) ||
                PerformanceSettingsFragment.class.getName().equals(fragmentName) ||
                SettingsFragment.class.getName().equals(fragmentName);
    }

    public static class SettingsFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_general);

            ListPreference fontSizePreference = (ListPreference) findPreference("font_size");
            if (fontSizePreference == null) {
                return;
            }
            fontSizePreference.setSummary(fontSizePreference.getValue() + " pt");
            fontSizePreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    preference.setSummary(newValue + " pt");
                    return true;
                }
            });

            ListPreference longTapPreference = (ListPreference) findPreference("long_tap");
            if (longTapPreference == null) {
                return;
            }
            longTapPreference.setSummary(longTapPreference.getEntry());
            longTapPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    ListPreference listPreference = (ListPreference) preference;
                    int listId = listPreference.findIndexOfValue((String) newValue);
                    CharSequence[] entries;
                    entries = listPreference.getEntries();
                    if (entries != null) {
                        preference.setSummary(entries[listId]);
                    }
                    return true;
                }
            });

            ListPreference themePreference = (ListPreference) findPreference("theme_name");
            if (themePreference == null) {
                return;
            }
            themePreference.setSummary(themePreference.getEntry());
            themePreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    ListPreference listPreference = (ListPreference) preference;
                    int listId = listPreference.findIndexOfValue((String) newValue);
                    CharSequence[] entries;
                    entries = listPreference.getEntries();
                    if (entries != null) {
                        preference.setSummary(entries[listId]);
                    }
//                    FragmentManager fragmentManager = getFragmentManager();
//                    if (fragmentManager != null) {
//                        new ThemeSwitchDialogFragment().show(fragmentManager, "dialog");
//                    }
                    return true;
                }
            });

            Preference about = findPreference("about");
            if (about == null) {
                return;
            }

            about.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    Intent intent = new Intent(getActivity(), LicenceActivity.class);
                    startActivity(intent);
                    return true;
                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
