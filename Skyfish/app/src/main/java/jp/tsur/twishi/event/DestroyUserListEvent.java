package jp.tsur.twishi.event;

public class DestroyUserListEvent {

    private final long mUserListId;

    public DestroyUserListEvent(final long userListId) {
        mUserListId = userListId;
    }

    public long getUserListId() {
        return mUserListId;
    }
}
