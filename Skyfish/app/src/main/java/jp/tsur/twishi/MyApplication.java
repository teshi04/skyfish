package jp.tsur.twishi;

import android.app.Application;
import android.content.res.Configuration;
import android.graphics.Typeface;

import jp.tsur.twishi.model.BasicSettings;
import jp.tsur.twishi.util.ImageUtil;


public class MyApplication extends Application {

    private static MyApplication sApplication;
    private static Typeface sFontello;

    @Override
    public void onCreate() {
        super.onCreate();
        sApplication = this;
        sFontello = Typeface.createFromAsset(getAssets(), "fontello.ttf");

        BasicSettings.init();
        ImageUtil.init();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    // 正しいのかなこれ
    public static MyApplication getApplication() {
        return sApplication;
    }

    public static Typeface getFontello() {
        return sFontello;
    }
}
