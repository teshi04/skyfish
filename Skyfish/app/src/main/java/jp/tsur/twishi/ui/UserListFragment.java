package jp.tsur.twishi.ui;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.ListView;

import jp.tsur.twishi.R;
import jp.tsur.twishi.adapter.UserListAdapter;
import jp.tsur.twishi.task.UserListsLoader;
import jp.tsur.twishi.ui.profile.ProfileActivity;
import twitter4j.ResponseList;
import twitter4j.UserList;


/**
 * 保存しているリスト
 */
public class UserListFragment extends ListFragment
        implements LoaderManager.LoaderCallbacks<ResponseList<UserList>> {

    private UserListAdapter mAdapter;

    public static UserListFragment newInstance(long userId) {
        UserListFragment f = new UserListFragment();
        Bundle args = new Bundle();
        args.putLong(ProfileActivity.EXTRA_USER_ID, userId);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mAdapter = new UserListAdapter(getActivity(), R.layout.list_item_user_list);
        setListAdapter(mAdapter);
        setListShown(false);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
//        StatusMenuFragment.newInstance(mAdapter.getItem(position))
//                .show(getActivity().getSupportFragmentManager(), "dialog");
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle args = new Bundle();
        args.putLong(ProfileActivity.EXTRA_USER_ID, getArguments().getLong("user_id"));
        getLoaderManager().initLoader(0, args, this).forceLoad();
    }

    @Override
    public Loader<ResponseList<UserList>> onCreateLoader(int id, Bundle args) {
        return new UserListsLoader(getActivity(), args.getLong("user_id"));
    }

    @Override
    public void onLoadFinished(Loader<ResponseList<UserList>> loader, ResponseList<UserList> userLists) {
        setListShown(true);

        if (userLists != null) {
            mAdapter.addAll(userLists);
        }
    }

    @Override
    public void onLoaderReset(Loader<ResponseList<UserList>> loader) {

    }

}
