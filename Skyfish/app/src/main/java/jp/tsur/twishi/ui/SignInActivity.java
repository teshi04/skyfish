package jp.tsur.twishi.ui;


import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import jp.tsur.twishi.AccessTokenManager;
import jp.tsur.twishi.R;
import jp.tsur.twishi.TwitterManager;
import jp.tsur.twishi.ui.main.MainActivity;
import jp.tsur.twishi.util.MessageUtil;
import jp.tsur.twishi.widget.FontelloButton;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;


public class SignInActivity extends ActionBarActivity {

    private static final String STATE_REQUEST_TOKEN = "request_token";
    private static final String QUERY_PARAMETER_OAUTH_VERIFIER = "oauth_verifier";
    private RequestToken mRequestToken;

    @InjectView(R.id.start_oauth_button)
    FontelloButton mStartOauthButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        ButterKnife.inject(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }

        Intent intent = getIntent();

        // バックグランドプロセスが死んでいるとonNewIntentでなくonCreateからoauth_verifierが来る
        if (savedInstanceState != null) {
            mRequestToken = (RequestToken) savedInstanceState.get(STATE_REQUEST_TOKEN);
            if (mRequestToken != null) {
                if (intent.getData() != null) {
                    String oauthVerifier = intent.getData().getQueryParameter(QUERY_PARAMETER_OAUTH_VERIFIER);
                    if (oauthVerifier != null && !oauthVerifier.isEmpty()) {
                        mStartOauthButton.setVisibility(View.GONE);
                        MessageUtil.showProgressDialog(this, getString(R.string.progress_process));
                        new VerifyOAuthTask().execute(oauthVerifier);
                    }
                }
            }
        }
    }

    @SuppressWarnings("NullableProblems")
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (mRequestToken != null) {
            outState.putSerializable(STATE_REQUEST_TOKEN, mRequestToken);
        }
    }

    @SuppressWarnings("NullableProblems")
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        mRequestToken = (RequestToken) savedInstanceState.getSerializable(STATE_REQUEST_TOKEN);
    }

    @Override
    public void onNewIntent(Intent intent) {
        if (intent.getData() == null
                || !intent.getData().toString().startsWith(getString(R.string.twitter_callback_url))) {
            return;
        }
        String oauth_verifier = intent.getData().getQueryParameter(QUERY_PARAMETER_OAUTH_VERIFIER);
        if (oauth_verifier == null || oauth_verifier.isEmpty()) {
            return;
        }
        MessageUtil.showProgressDialog(this, getString(R.string.progress_process));
        new VerifyOAuthTask().execute(oauth_verifier);
    }

    /**
     * この画面を終了してメイン画面だけのスタックを作成
     */
    private void moveToMain() {
        Intent intent = new Intent(this, MainActivity.class);
        TaskStackBuilder builder = TaskStackBuilder.create(this);
        builder.addNextIntent(intent);
        builder.startActivities();
        finish();
    }

    private class VerifyOAuthTask extends AsyncTask<String, Void, User> {
        @Override
        protected User doInBackground(String... params) {
            try {
                Twitter twitter = TwitterManager.getTwitterInstance();
                AccessToken accessToken = twitter.getOAuthAccessToken(mRequestToken, params[0]);
                AccessTokenManager.setAccessToken(accessToken);
                twitter.setOAuthAccessToken(accessToken);
                return twitter.verifyCredentials();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(User user) {
            MessageUtil.dismissProgressDialog();
            if (user != null) {
                // UserIconManager.addUserIconMap(user);
                MessageUtil.showToast(R.string.toast_sign_in_success);
                moveToMain();
            }
        }
    }


    @OnClick(R.id.start_oauth_button)
    void startOAuth() {
        MessageUtil.showProgressDialog(this, getString(R.string.progress_process));
        AsyncTask<Void, Void, RequestToken> task = new AsyncTask<Void, Void, RequestToken>() {
            @Override
            protected RequestToken doInBackground(Void... params) {
                try {
                    Twitter twitter = TwitterManager.getTwitterInstance();
                    return twitter.getOAuthRequestToken(getString(R.string.twitter_callback_url));
                } catch (TwitterException e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(RequestToken token) {
                MessageUtil.dismissProgressDialog();
                if (token == null) {
                    MessageUtil.showToast(R.string.toast_connection_failure);
                    return;
                }
                final String url = token.getAuthorizationURL();
                if (url == null) {
                    MessageUtil.showToast(R.string.toast_get_authorization_url_failure);
                    return;
                }
                mRequestToken = token;
                mStartOauthButton.setVisibility(View.GONE);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
            }
        };
        task.execute();
    }
}
