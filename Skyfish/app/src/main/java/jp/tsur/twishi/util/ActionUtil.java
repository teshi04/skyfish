package jp.tsur.twishi.util;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;

import de.greenrobot.event.EventBus;
import jp.tsur.twishi.AccessTokenManager;
import jp.tsur.twishi.FavRetweetManager;
import jp.tsur.twishi.R;
import jp.tsur.twishi.event.OpenEditorEvent;
import jp.tsur.twishi.task.DestroyDirectMessageTask;
import jp.tsur.twishi.task.DestroyFavoriteTask;
import jp.tsur.twishi.task.DestroyRetweetTask;
import jp.tsur.twishi.task.DestroyStatusTask;
import jp.tsur.twishi.task.FavoriteTask;
import jp.tsur.twishi.task.RetweetTask;
import jp.tsur.twishi.ui.UpdateStatusFragment;
import jp.tsur.twishi.ui.main.MainActivity;
import twitter4j.DirectMessage;
import twitter4j.UserMentionEntity;

// TODO:
public class ActionUtil {
    public static void doFavorite(Long statusId) {
        new FavoriteTask(statusId).execute();
    }

    public static void doDestroyFavorite(Long statusId) {
        new DestroyFavoriteTask(statusId).execute();
    }

    public static void doDestroyStatus(Long statusId) {
        new DestroyStatusTask(statusId).execute();
    }

    public static void doRetweet(Long statusId) {
        new RetweetTask(statusId).execute();
    }

    public static void doDestroyRetweet(twitter4j.Status status) {
        if (status.getUser().getId() == AccessTokenManager.getUserId()) {
            // 自分がRTしたStatus
            twitter4j.Status retweet = status.getRetweetedStatus();
            if (retweet != null) {
                new DestroyRetweetTask(retweet.getId(), status.getId()).execute();
            }
        } else {
            // 他人のStatusで、それを自分がRTしている

            // 被リツイート
            Long retweetedStatusId = -1L;

            // リツイート
            Long statusId = FavRetweetManager.getRtId(status.getId());
            if (statusId != null && statusId > 0) {
                // そのStatusそのものをRTしている
                retweetedStatusId = status.getId();
            } else {
                twitter4j.Status retweet = status.getRetweetedStatus();
                if (retweet != null) {
                    statusId = FavRetweetManager.getRtId(retweet.getId());
                    if (statusId != null && statusId > 0) {
                        // そのStatusがRTした元StatusをRTしている
                        retweetedStatusId = retweet.getId();
                    }
                }
            }

            // TODO:
            if (statusId != null && statusId == 0L) {
                // 処理中は 0
                MessageUtil.showToast(R.string.toast_destroy_retweet_progress);
            } else if (statusId != null && statusId > 0 && retweetedStatusId > 0) {
                new DestroyRetweetTask(retweetedStatusId, statusId).execute();
            }
        }
    }

    public static void doReply(twitter4j.Status status, Activity context) {
        Long userId = AccessTokenManager.getUserId();
        UserMentionEntity[] mentions = status.getUserMentionEntities();
        String text;
        if (status.getUser().getId() == userId && mentions.length == 1) {
            text = "@" + mentions[0].getScreenName() + " ";
        } else {
            text = "@" + status.getUser().getScreenName() + " ";
        }
        if (context instanceof MainActivity) {
            EventBus.getDefault().post(new OpenEditorEvent(text, status, text.length(), text.length()));
        } else {
            DialogFragment dialogFragment =
                    UpdateStatusFragment.newInstance(status, text, text.length(), 0);
            dialogFragment.show(context.getFragmentManager(), "update_status_dialog");
        }
    }

    public static void doReplyAll(twitter4j.Status status, Activity context) {
        long userId = AccessTokenManager.getUserId();
        UserMentionEntity[] mentions = status.getUserMentionEntities();
        String text = "";
        int selection = 0;
        if (status.getUser().getId() != userId) {
            text = "@" + status.getUser().getScreenName() + " ";
            selection = text.length();
        }
        for (UserMentionEntity mention : mentions) {
            if (status.getUser().getId() == mention.getId()) {
                continue;
            }
            if (userId == mention.getId()) {
                continue;
            }
            text = text.concat("@" + mention.getScreenName() + " ");
        }
        if (context instanceof MainActivity) {
            EventBus.getDefault().post(new OpenEditorEvent(text, status, selection, text.length()));
        } else {
            DialogFragment dialogFragment =
                    UpdateStatusFragment.newInstance(status, text, text.length(), text.length());
            dialogFragment.show(context.getFragmentManager(), "update_status_dialog");
        }
    }

    // TODO: DってつけてDMにするのやめる
    public static void doReplyDirectMessage(DirectMessage directMessage, Context context) {
//        String text;
//        if (AccessTokenManager.getUserId() == directMessage.getSender().getId()) {
//            text = "D " + directMessage.getRecipient().getScreenName() + " ";
//        } else {
//            text = "D " + directMessage.getSender().getScreenName() + " ";
//        }
//        if (context instanceof MainActivity) {
//            EventBus.getDefault().post(new OpenEditorEvent(text, null, text.length(), null));
//       } else {
//            Intent intent = new Intent(context, PostActivity.class);
//            intent.putExtra("status", text);
//            intent.putExtra("selection", text.length());
//            context.startActivity(intent);
//        }
    }

    public static void doDestroyDirectMessage(long id) {
        new DestroyDirectMessageTask().execute(id);
    }

    public static void doQuote(twitter4j.Status status, Activity context) {
        String text = " https://twitter.com/"
                + status.getUser().getScreenName()
                + "/status/" + String.valueOf(status.getId());
        if (context instanceof MainActivity) {
            EventBus.getDefault().post(new OpenEditorEvent(text, null, 0, 0));
        } else {
            DialogFragment dialogFragment =
                    UpdateStatusFragment.newInstance(null, text, text.length(), 0);
            dialogFragment.show(context.getFragmentManager(), "update_status_dialog");
        }
    }
}
