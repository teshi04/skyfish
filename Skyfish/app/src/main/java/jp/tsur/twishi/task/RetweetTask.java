package jp.tsur.twishi.task;

import android.os.AsyncTask;

import jp.tsur.twishi.FavRetweetManager;
import jp.tsur.twishi.R;
import jp.tsur.twishi.TwitterManager;
import jp.tsur.twishi.util.MessageUtil;
import jp.tsur.twishi.util.TwitterUtil;
import twitter4j.TwitterException;

//TODO:

/**
 * リツイートする
 */
public class RetweetTask extends AsyncTask<Void, Void, TwitterException> {

    private long mStatusId;

    public RetweetTask(long statusId) {
        mStatusId = statusId;
        FavRetweetManager.setRtId(statusId, (long) 0);
//        EventBus.getDefault().post(new StatusActionEvent());
    }

    @Override
    protected TwitterException doInBackground(Void... params) {
        try {
            twitter4j.Status status = TwitterManager.getTwitter().retweetStatus(mStatusId);
            FavRetweetManager.setRtId(status.getRetweetedStatus().getId(), status.getId());
            return null;
        } catch (TwitterException e) {
            FavRetweetManager.setRtId(mStatusId, null);
            e.printStackTrace();
            return e;
        }
    }

    @Override
    protected void onPostExecute(TwitterException e) {
        if (e == null) {
            MessageUtil.showToast(R.string.toast_retweet_success);
        } else if (e.getErrorCode() == TwitterUtil.ERROR_CODE_RETWEET_ALREADY) {
            MessageUtil.showToast(R.string.toast_retweet_already);
        } else {
//            EventBus.getDefault().post(new StatusActionEvent());
            MessageUtil.showToast(R.string.toast_retweet_failure);
        }
    }
}