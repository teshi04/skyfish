package jp.tsur.twishi.ui.profile;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.ButterKnife;
import butterknife.InjectView;
import jp.tsur.twishi.R;
import twitter4j.URLEntity;
import twitter4j.User;

/**
 * プロフィール上部の右面
 */
public class DescriptionFragment extends Fragment {

    @InjectView(R.id.description)
    TextView mDescription;
    @InjectView(R.id.location)
    TextView mLocation;
    @InjectView(R.id.url)
    TextView mUrl;
    @InjectView(R.id.start)
    TextView mStart;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profile_description, container, false);
        ButterKnife.inject(this, v);

        final User user = (User) getArguments().getSerializable("user");

        assert user != null;
        if (!TextUtils.isEmpty(user.getDescription())) {
            String descriptionString = user.getDescription();

            if (user.getDescriptionURLEntities() != null) {
                URLEntity[] urls = user.getDescriptionURLEntities();
                for (URLEntity descriptionUrl : urls) {
                    Pattern p = Pattern.compile(descriptionUrl.getURL());
                    Matcher m = p.matcher(descriptionString);
                    descriptionString = m.replaceAll(descriptionUrl.getExpandedURL());
                }
            }
            mDescription.setText(descriptionString);
        } else {
            mDescription.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(user.getLocation())) {
            mLocation.setText(user.getLocation());
        } else {
            mLocation.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(user.getURL())) {
            if (user.getURLEntity() != null) {
                mUrl.setText(user.getURLEntity().getExpandedURL());
            } else {
                mUrl.setText(user.getURL());
            }
        } else {
            mUrl.setVisibility(View.GONE);
        }

        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(getString(R.string.format_user_created_at), Locale.ENGLISH);
        mStart.setText(simpleDateFormat.format(user.getCreatedAt()));

        return v;
    }
}
