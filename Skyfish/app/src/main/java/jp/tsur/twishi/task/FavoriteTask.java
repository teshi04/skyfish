package jp.tsur.twishi.task;

import android.os.AsyncTask;

import jp.tsur.twishi.FavRetweetManager;
import jp.tsur.twishi.R;
import jp.tsur.twishi.TwitterManager;
import jp.tsur.twishi.util.MessageUtil;
import jp.tsur.twishi.util.TwitterUtil;
import twitter4j.TwitterException;

/**
 * お気に入りに登録
 */
public class FavoriteTask extends AsyncTask<Void, Void, TwitterException> {

    private long mStatusId;

    public FavoriteTask(long statusId) {
        mStatusId = statusId;

        // 先にsetFavしておかないとViewの星が戻ってしまう、
        // 重複エラー以外の理由で失敗し場合（通信エラー等）は戻す
        FavRetweetManager.setFav(mStatusId);
        //EventBus.getDefault().post(new StatusActionEvent());
    }

    @Override
    protected TwitterException doInBackground(Void... params) {
        try {
            TwitterManager.getTwitter().createFavorite(mStatusId);
        } catch (TwitterException e) {
            return e;
        }
        return null;
    }

    @Override
    protected void onPostExecute(TwitterException e) {
        if (e == null) {
            MessageUtil.showToast(R.string.toast_favorite_success);
        } else if (e.getErrorCode() == TwitterUtil.ERROR_CODE_FAVORITE_ALREADY) {
            MessageUtil.showToast(R.string.toast_favorite_already);
        } else {
            FavRetweetManager.removeFav(mStatusId);
//            EventBus.getDefault().post(new StatusActionEvent());
            MessageUtil.showToast(R.string.toast_favorite_failure);
        }
    }
}
