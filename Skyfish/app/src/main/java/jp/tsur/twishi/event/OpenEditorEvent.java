package jp.tsur.twishi.event;

import twitter4j.Status;

public class OpenEditorEvent {

    private final String mText;
    private final Status mInReplyToStatus;
    private final int mSelection;
    private final int mSelectionEnd;

    public OpenEditorEvent(final String text, final Status inReplyToStatus, final int selection, final int selectionEnd) {
        mText = text;
        mInReplyToStatus = inReplyToStatus;
        mSelection = selection;
        mSelectionEnd = selectionEnd;
    }

    public String getText() {
        return mText;
    }

    public Status getInReplyToStatus() {
        return mInReplyToStatus;
    }

    public Integer getSelection() {
        return mSelection;
    }

    public Integer getSelectionEnd() {
        return mSelectionEnd;
    }
}
