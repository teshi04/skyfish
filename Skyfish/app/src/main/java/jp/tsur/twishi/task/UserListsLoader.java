package jp.tsur.twishi.task;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import jp.tsur.twishi.TwitterManager;
import twitter4j.ResponseList;
import twitter4j.UserList;

/**
 * 購読しているリストを取得
 */
public class UserListsLoader extends AsyncTaskLoader<ResponseList<UserList>> {

    private long mUserId;

    public UserListsLoader(Context context, long userId) {
        super(context);
        mUserId = userId;
    }

    @Override
    public ResponseList<UserList> loadInBackground() {
        try {
            return TwitterManager.getTwitter().getUserLists(mUserId);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
