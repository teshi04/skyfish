package jp.tsur.twishi.task;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import jp.tsur.twishi.TwitterManager;
import twitter4j.PagableResponseList;
import twitter4j.UserList;

/**
 * ユーザが追加されているリスト取得
 */
public class UserListMembershipsLoader extends AsyncTaskLoader<PagableResponseList<UserList>> {

    private long mUserId;
    private long mCursor;

    public UserListMembershipsLoader(Context context, long userId, long cursor) {
        super(context);
        mUserId = userId;
        mCursor = cursor;
    }

    @Override
    public PagableResponseList<UserList> loadInBackground() {
        try {
            return TwitterManager.getTwitter().getUserListMemberships(mUserId, mCursor);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}