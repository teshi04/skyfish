package jp.tsur.twishi.ui.profile;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;

import de.greenrobot.event.EventBus;
import jp.tsur.twishi.R;
import jp.tsur.twishi.adapter.StatusAdapter;
import jp.tsur.twishi.event.StatusActionEvent;
import jp.tsur.twishi.event.StreamingDestroyStatusEvent;
import jp.tsur.twishi.model.Row;
import jp.tsur.twishi.task.UserTimelineLoader;
import jp.tsur.twishi.ui.status.StatusMenuFragment;
import twitter4j.ResponseList;
import twitter4j.Status;
import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;


/**
 * ユーザーのタイムライン
 */
public class UserTimelineFragment extends ListFragment implements OnRefreshListener,
        LoaderManager.LoaderCallbacks<ResponseList<Status>> {

    private StatusAdapter mAdapter;
    private ListView mListView;
    private View mFooter;
    private long mUserId;
    private boolean mAutoLoader;
    private boolean mReload;
    private long mMaxId;
    private PullToRefreshLayout mPullToRefreshLayout;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mListView = getListView();

        LayoutInflater inflater = LayoutInflater.from(getActivity());
        mFooter = inflater.inflate(R.layout.guruguru, mListView, false);
        mListView.addFooterView(mFooter, null, false);
        mAdapter = new StatusAdapter(getActivity(), R.layout.list_item_status);
        setListAdapter(mAdapter);
        setListShown(false);

        ViewGroup viewGroup = (ViewGroup) view;

        mPullToRefreshLayout = new PullToRefreshLayout(viewGroup.getContext());

        ActionBarPullToRefresh.from(getActivity())
                .insertLayoutInto(viewGroup)
                .theseChildrenArePullable(android.R.id.list, android.R.id.empty)
                .listener(this)
                .setup(mPullToRefreshLayout);

        mUserId = getArguments().getLong(ProfileActivity.EXTRA_USER_ID);

        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (totalItemCount == firstVisibleItem + visibleItemCount) {
                    additionalReading();
                }
            }
        });
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        StatusMenuFragment.newInstance(mAdapter.getItem(position))
                .show(getActivity().getSupportFragmentManager(), "dialog");
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(0, getArguments(), this).forceLoad();
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    public void onEventMainThread(StatusActionEvent event) {
        mAdapter.notifyDataSetChanged();
    }

    public void onEventMainThread(StreamingDestroyStatusEvent event) {
        mAdapter.removeStatus(event.getStatusId());
    }

    @Override
    public void onRefreshStarted(View view) {
        mReload = true;
        mMaxId = 0;
        Bundle args = new Bundle();
        args.putLong(ProfileActivity.EXTRA_USER_ID, mUserId);
        getLoaderManager().restartLoader(0, args, this).forceLoad();
    }

    private void additionalReading() {
        if (!mAutoLoader || mReload) {
            return;
        }
        mAutoLoader = false;
        Bundle args = new Bundle();
        args.putLong(ProfileActivity.EXTRA_USER_ID, mUserId);
        args.putLong("max_id", mMaxId);
        getLoaderManager().restartLoader(0, args, this).forceLoad();
    }

    @Override
    public Loader<ResponseList<Status>> onCreateLoader(int id, Bundle args) {
        return new UserTimelineLoader(getActivity(), args.getLong(ProfileActivity.EXTRA_USER_ID), args.getLong("max_id", 0L));
    }

    @Override
    public void onLoadFinished(Loader<ResponseList<Status>> loader, ResponseList<Status> statuses) {
        setListShown(true);
        if (statuses == null || statuses.size() == 0) {
            mListView.removeFooterView(mFooter);
            return;
        }

        // 引っ張って更新
        if (mReload) {
            mAdapter.clear();
            mReload = false;
            mPullToRefreshLayout.setRefreshComplete();
        }

        for (Status status : statuses) {
            mAdapter.add(Row.newStatus(status));
        }
        mAdapter.notifyDataSetChanged();
        mMaxId = statuses.get(statuses.size() - 1).getId();
        mAutoLoader = true;
    }

    @Override
    public void onLoaderReset(Loader<ResponseList<Status>> loader) {

    }
}
