package jp.tsur.twishi.task;

import android.os.AsyncTask;

import jp.tsur.twishi.TwitterManager;

/**
 * リストの購読を解除
 */
public class DestroyUserListSubscriptionTask extends AsyncTask<Long, Void, Boolean> {

    @Override
    protected Boolean doInBackground(Long... params) {
        try {
            TwitterManager.getTwitter().destroyUserListSubscription(params[0]);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
