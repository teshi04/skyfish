package jp.tsur.twishi.task;

import android.os.AsyncTask;

import jp.tsur.twishi.TwitterManager;

/**
 * リストを削除
 */
public class DestroyUserListTask extends AsyncTask<Long, Void, Boolean> {

    @Override
    protected Boolean doInBackground(Long... params) {
        try {
            TwitterManager.getTwitter().destroyUserList(params[0]);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
