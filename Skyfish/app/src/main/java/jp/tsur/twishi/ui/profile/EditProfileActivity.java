package jp.tsur.twishi.ui.profile;


import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.File;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import jp.tsur.twishi.R;
import jp.tsur.twishi.task.UpdateProfileTask;
import jp.tsur.twishi.util.ImageUtil;
import jp.tsur.twishi.util.MessageUtil;
import twitter4j.User;

public class EditProfileActivity extends ActionBarActivity {

    private static final int REQ_PICK_PROFILE_IMAGE = 1;
    public static final String EXTRA_USER = "user";

    @InjectView(R.id.icon)
    ImageView mIcon;
    @InjectView(R.id.name)
    EditText mName;
    @InjectView(R.id.location)
    EditText mLocation;
    @InjectView(R.id.url)
    EditText mUrl;
    @InjectView(R.id.description)
    EditText mDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        JustawayApplication.getApplication().setTheme(this);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.inject(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        final User user = (User) getIntent().getSerializableExtra(EXTRA_USER);
        mName.setText(user.getName());
        mLocation.setText(user.getLocation());
        mUrl.setText(user.getURLEntity().getExpandedURL());
        mDescription.setText(user.getDescription());
        ImageUtil.displayRoundedImage(user.getOriginalProfileImageURL(), mIcon);
    }

    @OnClick(R.id.icon_container)
    void updateProfileImage() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, REQ_PICK_PROFILE_IMAGE);
    }

    private File uriToFile(Uri uri) {
        ContentResolver cr = getContentResolver();
        String[] columns = {MediaStore.Images.Media.DATA};
        Cursor c = cr.query(uri, columns, null, null, null);
        assert c != null;
        c.moveToFirst();
        final String fileName = c.getString(0);
        if (fileName == null) {
            MessageUtil.showToast(getString(R.string.toast_set_image_failure));
            return null;
        }
        final File path = new File(fileName);

        if (!path.exists()) {
            return null;
        }

        return path;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQ_PICK_PROFILE_IMAGE:
                if (resultCode == RESULT_OK) {
                    Uri uri = data.getData();
                    File file = uriToFile(uri);
                    if (file != null) {
//                        UpdateProfileImageFragment dialog = UpdateProfileImageFragment.newInstance(file, uri);
//                        dialog.show(getSupportFragmentManager(), "dialog");
                    }
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.save:
                MessageUtil.showProgressDialog(this, getString(R.string.progress_process));
                new UpdateProfileTask() {
                    @Override
                    protected void onPostExecute(User user) {
                        MessageUtil.dismissProgressDialog();
                        if (user != null) {
                            MessageUtil.showToast(R.string.toast_update_profile_success);
                            setResult(RESULT_OK);
                            finish();
                        } else {
                            MessageUtil.showToast(R.string.toast_update_profile_failure);
                        }
                    }
                }.execute(mName.getText().toString(), mUrl.getText().toString(),
                        mLocation.getText().toString(), mDescription.getText().toString());
                break;
        }
        return true;
    }
}