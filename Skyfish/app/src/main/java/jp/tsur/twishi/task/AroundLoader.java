//package jp.tsur.twishi.task;
//
//import android.content.Context;
//import android.support.v4.content.AsyncTaskLoader;
//
//import java.util.List;
//
//import jp.tsur.twishi.TwitterManager;
//import twitter4j.Paging;
//import twitter4j.ResponseList;
//import twitter4j.Status;
//
//
//public class AroundStatusLoader extends AsyncTaskLoader<List<Status>> {
//
//    private long mUserId;
//    private long mMaxId;
//
//    public AroundStatusLoader(Context context, long userId, long maxId) {
//        super(context);
//        mUserId = userId;
//    }
//
//
//    @Override
//    public List<twitter4j.Status> loadInBackground() {
//        try {
//            Paging paging = new Paging();
//            if (mMaxId > 0) {
//                paging.setMaxId(mMaxId - 1);
//                paging.setCount(100);//BasicSettings.getPageCount());
//            }
//            return TwitterManager.getTwitter().getUserTimeline(mUserId, paging);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
//}