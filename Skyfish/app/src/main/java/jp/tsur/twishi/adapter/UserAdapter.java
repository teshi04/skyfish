package jp.tsur.twishi.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import jp.tsur.twishi.R;
import jp.tsur.twishi.util.ImageUtil;
import jp.tsur.twishi.widget.FontelloTextView;
import twitter4j.URLEntity;
import twitter4j.User;

public class UserAdapter extends ArrayAdapter<User> {

    static class ViewHolder {
        @InjectView(R.id.icon_image)
        ImageView mIcon;
        @InjectView(R.id.display_name)
        TextView mDisplayName;
        @InjectView(R.id.screen_name_label)
        TextView mScreenName;
        @InjectView(R.id.lock_label)
        FontelloTextView mFontelloLock;
        @InjectView(R.id.description)
        TextView mDescription;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }

    private LayoutInflater mInflater;
    private int mLayout;

    public UserAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mLayout = textViewResourceId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        // ビューを受け取る
        View view = convertView;
        if (view == null) {
            // 受け取ったビューがnullなら新しくビューを生成
            view = mInflater.inflate(this.mLayout, null);
            if (view == null) {
                return null;
            }
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        final User user = getItem(position);

        String iconUrl = user.getBiggerProfileImageURL();
        ImageUtil.displayRoundedImage(iconUrl, holder.mIcon);

        holder.mDisplayName.setText(user.getName());
        holder.mScreenName.setText("@" + user.getScreenName());

        String descriptionString = user.getDescription();
        if (descriptionString != null && descriptionString.length() > 0) {
            URLEntity[] urls = user.getDescriptionURLEntities();
            for (URLEntity descriptionUrl : urls) {
                descriptionString = descriptionString.replaceAll(descriptionUrl.getURL(),
                        descriptionUrl.getExpandedURL());
            }
            holder.mDescription.setText(descriptionString);
        } else {
            holder.mDescription.setVisibility(View.GONE);
        }

        if (user.isProtected()) {
            holder.mFontelloLock.setVisibility(View.GONE);
        }

        return view;
    }
}
