package jp.tsur.twishi.util;


import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

import jp.tsur.twishi.MyApplication;

public class MessageUtil {
    private static ProgressDialog sProgressDialog;

    public static void showToast(String text) {
        MyApplication application = MyApplication.getApplication();
        Toast.makeText(application, text, Toast.LENGTH_SHORT).show();
    }

    public static void showToast(int id) {
        MyApplication application = MyApplication.getApplication();
        String text = application.getString(id);
        Toast.makeText(application, text, Toast.LENGTH_SHORT).show();
    }

    public static void showProgressDialog(Context context, String message) {
        sProgressDialog = new ProgressDialog(context);
        sProgressDialog.setMessage(message);
        sProgressDialog.setCanceledOnTouchOutside(false);
        sProgressDialog.show();
    }

    public static void dismissProgressDialog() {
        if (sProgressDialog != null)
            try {
                sProgressDialog.dismiss();
            } finally {
                sProgressDialog = null;
            }
    }
}