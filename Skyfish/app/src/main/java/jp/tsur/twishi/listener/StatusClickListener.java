package jp.tsur.twishi.listener;

import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.HeaderViewListAdapter;

import jp.tsur.twishi.adapter.StatusAdapter;
import jp.tsur.twishi.ui.status.StatusMenuFragment;


public class StatusClickListener implements AdapterView.OnItemClickListener {

    private FragmentActivity mFragmentActivity;

    public StatusClickListener(FragmentActivity fragmentActivity) {
        mFragmentActivity = fragmentActivity;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        HeaderViewListAdapter headerViewListAdapter = (HeaderViewListAdapter) adapterView.getAdapter();
        StatusAdapter adapter = (StatusAdapter) headerViewListAdapter.getWrappedAdapter();
        StatusMenuFragment.newInstance(adapter.getItem(i))
                .show(mFragmentActivity.getSupportFragmentManager(), "dialog");
    }
}