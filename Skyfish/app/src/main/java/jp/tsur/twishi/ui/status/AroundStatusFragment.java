package jp.tsur.twishi.ui.status;

import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.widget.ListView;

import java.util.List;

import jp.tsur.twishi.R;
import jp.tsur.twishi.TwitterManager;
import jp.tsur.twishi.adapter.StatusAdapter;
import jp.tsur.twishi.listener.StatusClickListener;
import jp.tsur.twishi.listener.StatusLongClickListener;
import jp.tsur.twishi.model.Row;
import jp.tsur.twishi.util.MessageUtil;
import twitter4j.Paging;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;

/**
 * 会話を表示
 */
public class AroundStatusFragment extends DialogFragment {

    private StatusAdapter mAdapter;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        FragmentActivity activity = getActivity();
        Dialog dialog = new Dialog(activity, R.style.NoTitleDialog);
        final ListView listView = new ListView(getActivity());
        dialog.setContentView(listView);

        mAdapter = new StatusAdapter(activity, R.layout.list_item_status);
        listView.setAdapter(mAdapter);

        listView.setOnItemClickListener(new StatusClickListener(activity));

        listView.setOnItemLongClickListener(new StatusLongClickListener(activity));

        return dialog;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Status status = (Status) getArguments().getSerializable("status");
        mAdapter.add(Row.newStatus(status));
        new BeforeStatusTask().execute(status);
    }

    //    @Override
//    public void onResume() {
//        super.onResume();
//        EventBus.getDefault().register(this);
//    }
//
//    @Override
//    public void onPause() {
//        EventBus.getDefault().unregister(this);
//        super.onPause();
//    }
//
//    @SuppressWarnings("UnusedDeclaration")
//    public void onEventMainThread(StatusActionEvent event) {
//        mAdapter.notifyDataSetChanged();
//    }
//
//    @SuppressWarnings("UnusedDeclaration")
//    public void onEventMainThread(StreamingDestroyStatusEvent event) {
//        mAdapter.removeStatus(event.getStatusId());
//    }

    private class BeforeStatusTask extends AsyncTask<Status, Void, ResponseList<Status>> {

        public BeforeStatusTask() {
            super();
        }

        @Override
        protected ResponseList<twitter4j.Status> doInBackground(twitter4j.Status... params) {
            try {
                Twitter twitter = TwitterManager.getTwitter();
                twitter4j.Status status = params[0];
                Paging paging = new Paging();
                paging.setCount(3);
                paging.setMaxId(status.getId() - 1);
                return twitter.getUserTimeline(status.getUser().getScreenName(), paging);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(ResponseList<twitter4j.Status> statuses) {
            if (statuses != null) {
                if (statuses.size() > 0) {
                    for (twitter4j.Status status : statuses) {
                        mAdapter.add(Row.newStatus(status));
                    }
                    mAdapter.notifyDataSetChanged();
                    new AfterStatusTask().execute(statuses.get(0));
                }
            } else {
                MessageUtil.showToast(R.string.toast_load_data_failure);
            }
        }
    }

    private class AfterStatusTask extends AsyncTask<Status, Void, List<Status>> {

        public AfterStatusTask() {
            super();
        }

        @Override
        protected List<twitter4j.Status> doInBackground(twitter4j.Status... params) {
            try {
                Twitter twitter = TwitterManager.getTwitter();
                twitter4j.Status status = params[0];
                Paging paging = new Paging();
                paging.setCount(200);
                paging.setSinceId(status.getId() - 1);
                for (int page = 1; page < 5; page++) {
                    paging.setPage(page);
                    ResponseList<twitter4j.Status> statuses = twitter.getUserTimeline(status.getUser().getScreenName(), paging);
                    int index = 0;
                    for (twitter4j.Status row : statuses) {
                        if (row.getId() == status.getId()) {
                            if (index > 0) {
                                return statuses.subList(Math.max(0, index - 4), index - 1);
                            }
                        }
                        index++;
                    }
                }
                return null;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<twitter4j.Status> statuses) {
            if (statuses != null) {
                int i = 0;
                for (twitter4j.Status status : statuses) {
                    mAdapter.insert(Row.newStatus(status), i);
                    i++;
                }
                mAdapter.notifyDataSetChanged();
            }
        }
    }
}
