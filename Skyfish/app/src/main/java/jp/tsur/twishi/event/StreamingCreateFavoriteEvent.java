package jp.tsur.twishi.event;


import jp.tsur.twishi.model.Row;

public class StreamingCreateFavoriteEvent {
    private final Row mRow;

    public StreamingCreateFavoriteEvent(Row row) {
        mRow = row;
    }

    public Row getRow() {
        return mRow;
    }
}
