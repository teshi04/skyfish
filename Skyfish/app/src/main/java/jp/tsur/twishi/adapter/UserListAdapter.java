package jp.tsur.twishi.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import jp.tsur.twishi.R;
import jp.tsur.twishi.ui.profile.ProfileActivity;
import jp.tsur.twishi.util.ImageUtil;
import twitter4j.UserList;

public class UserListAdapter extends ArrayAdapter<UserList> {

    static class ViewHolder {
        @InjectView(R.id.icon_image)
        ImageView mIcon;
        @InjectView(R.id.list_name)
        TextView mListName;
        @InjectView(R.id.screen_name_label)
        TextView mScreenName;
        @InjectView(R.id.description)
        TextView mDescription;
        @InjectView(R.id.member_count)
        TextView mMemberCount;
        @InjectView(R.id.lock_label)
        TextView mLockLabel;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }

    private Context mContext;
    private LayoutInflater mInflater;
    private int mLayout;

    public UserListAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = context;
        mLayout = textViewResourceId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // ビューを受け取る
        ViewHolder holder;
        View view = convertView;
        if (view == null) {
            // 受け取ったビューがnullなら新しくビューを生成
            view = mInflater.inflate(this.mLayout, null);
            if (view == null) {
                return null;
            }
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        final UserList userList = getItem(position);

        String iconUrl = userList.getUser().getBiggerProfileImageURL();
        ImageUtil.displayRoundedImage(iconUrl, holder.mIcon);

        holder.mListName.setText(userList.getName());
        holder.mScreenName.setText(mContext.getString(R.string.label_created_by, userList.getUser().getScreenName()));
        holder.mDescription.setText(userList.getDescription());
        holder.mMemberCount.setText(mContext.getString(R.string.label_members, userList.getMemberCount()));
        if (userList.isPublic()) {
            holder.mLockLabel.setVisibility(View.GONE);
        } else {
            holder.mLockLabel.setVisibility(View.VISIBLE);
        }

        holder.mIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ProfileActivity.class);
                intent.putExtra(ProfileActivity.EXTRA_USER_ID, userList.getUser().getId());
                mContext.startActivity(intent);
            }
        });

        return view;
    }
}
