package jp.tsur.twishi.ui.settings;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import jp.tsur.twishi.R;
import jp.tsur.twishi.TwitterManager;
import jp.tsur.twishi.util.MessageUtil;

public class StreamingSwitchDialogFragment extends DialogFragment {

    public static StreamingSwitchDialogFragment newInstance(boolean turnOn) {
        final Bundle args = new Bundle(1);
        args.putBoolean("turn_on", turnOn);

        final StreamingSwitchDialogFragment f = new StreamingSwitchDialogFragment();
        f.setArguments(args);
        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final boolean turnOn = getArguments().getBoolean("turn_on");

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(turnOn ? R.string.confirm_start_streaming : R.string.confirm_stop_streaming);
        builder.setPositiveButton(getString(R.string.button_ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if (turnOn) {
                            TwitterManager.startStreaming();
                            MessageUtil.showToast(R.string.toast_start_streaming);
                        } else {
                            TwitterManager.stopStreaming();
                            MessageUtil.showToast(R.string.toast_stop_streaming);
                        }
                        dismiss();
                    }
                }
        );
        builder.setNegativeButton(getString(R.string.button_cancel),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                    }
                }
        );
        return builder.create();
    }
}
