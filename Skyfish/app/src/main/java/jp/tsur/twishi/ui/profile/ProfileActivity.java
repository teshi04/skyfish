package jp.tsur.twishi.ui.profile;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.LoaderManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.HeaderViewListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.manuelpeinado.fadingactionbar.FadingActionBarHelper;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import jp.tsur.twishi.AccessTokenManager;
import jp.tsur.twishi.R;
import jp.tsur.twishi.adapter.StatusAdapter;
import jp.tsur.twishi.listener.StatusLongClickListener;
import jp.tsur.twishi.model.Profile;
import jp.tsur.twishi.model.ProfileResponse;
import jp.tsur.twishi.model.Row;
import jp.tsur.twishi.task.CreateBlockTask;
import jp.tsur.twishi.task.DestroyBlockTask;
import jp.tsur.twishi.task.DestroyFriendshipTask;
import jp.tsur.twishi.task.FollowTask;
import jp.tsur.twishi.task.ReportSpamTask;
import jp.tsur.twishi.task.UserLoader;
import jp.tsur.twishi.ui.ScaleImageActivity;
import jp.tsur.twishi.ui.UpdateStatusFragment;
import jp.tsur.twishi.ui.status.StatusMenuFragment;
import jp.tsur.twishi.util.ImageUtil;
import jp.tsur.twishi.util.MessageUtil;
import jp.tsur.twishi.widget.FontelloTextView;
import twitter4j.Relationship;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.URLEntity;
import twitter4j.User;

/**
 * プロフィール画面
 */
public class ProfileActivity extends FragmentActivity implements
        LoaderManager.LoaderCallbacks<ProfileResponse> {

    public static final String EXTRA_SCREEN_NAME = "screen_name";
    public static final String EXTRA_USER_ID = "user_id";
    private static final int REQUEST_TWITTER = 0;
    private static final int REQUEST_FAVSTER = 1;
    private static final int REQUEST_ACLOG = 2;
    private static final int REQUEST_TWILOG = 3;
    public static final int REQUEST_EDIT_PROFILE = 100;

    @InjectView(R.id.banner)
    ImageView mBanner;
    @InjectView(R.id.icon_image)
    ImageView mInconImage;
    @InjectView(R.id.name_label)
    TextView mNameLabel;
    @InjectView(R.id.lock_label)
    FontelloTextView mLockLabel;
    @InjectView(R.id.screen_name_label)
    TextView mScreenNameLabel;
    @InjectView(R.id.followed_by_label)
    TextView mFollowedByLabel;
    @InjectView(R.id.follow_botton)
    Button mFollowButton;
    @InjectView(R.id.list_button)
    FontelloTextView mListButton;
    @InjectView(R.id.description)
    TextView mDescription;
    @InjectView(R.id.location)
    TextView mLocation;
    @InjectView(R.id.url)
    TextView mUrl;
    @InjectView(R.id.start)
    TextView mStart;
    @InjectView(R.id.statuses_count)
    TextView mStatusesCount;
    @InjectView(R.id.friends_count)
    TextView mFriendsCount;
    @InjectView(R.id.followers_count)
    TextView mFollowersCount;
    @InjectView(R.id.favourites_count)
    TextView mFavouritesCount;
    @InjectView(R.id.progress_bar_container)
    LinearLayout mProgressBarContainer;
    @InjectView(R.id.main_container)
    LinearLayout mMainContainer;

    private User mUser;
    private StatusAdapter mAdapter;

    private boolean mFollowFlg;
    private boolean mBlocking;
    private ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

//        FadingActionBarHelper helper = new FadingActionBarHelper()
//                .actionBarBackground(R.drawable.ab_solid_dark_holo)
//                .headerOverlayLayout(R.layout.activity_profile_header_overlay)
//                .headerLayout(R.layout.activity_profile_header)
//                .contentLayout(R.layout.activity_listview);
//
//        setContentView(helper.createView(this));
        mListView = (ListView) findViewById(android.R.id.list);
        LayoutInflater inflater = LayoutInflater.from(this);
        View header = inflater.inflate(R.layout.activity_profile_contents, mListView, false);
        mListView.addHeaderView(header, null, false);
        mListView.setHeaderDividersEnabled(false);
        mListView.setFastScrollEnabled(true);

//        helper.initActionBar(this);
        ButterKnife.inject(this);

        mAdapter = new StatusAdapter(this, R.layout.list_item_status);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HeaderViewListAdapter headerViewListAdapter = (HeaderViewListAdapter) mListView.getAdapter();
                final StatusAdapter adapter = (StatusAdapter) headerViewListAdapter.getWrappedAdapter();
                StatusMenuFragment.newInstance(adapter.getItem(position - mListView.getHeaderViewsCount()))
                        .show(getSupportFragmentManager(), "dialog");
            }
        });
        mListView.setOnItemLongClickListener(new StatusLongClickListener(this, mListView.getHeaderViewsCount()));

        Intent intent = getIntent();
        Bundle args = new Bundle();
        if (Intent.ACTION_VIEW.equals(intent.getAction()) && intent.getData() != null) {
            args.putString(EXTRA_SCREEN_NAME, intent.getData().getLastPathSegment());
        } else {
            String screenName = intent.getStringExtra(EXTRA_SCREEN_NAME);
            if (screenName != null) {
                args.putString(EXTRA_SCREEN_NAME, screenName);
            } else {
                args.putLong(EXTRA_USER_ID, intent.getLongExtra(EXTRA_USER_ID, 0));
            }
        }

        getLoaderManager().initLoader(0, args, this).forceLoad();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        String text;
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.send_reply:
                text = "@" + mUser.getScreenName() + " ";
                DialogFragment dialogFragment =
                        UpdateStatusFragment.newInstance(null, text, text.length(), 0);
                dialogFragment.show(getFragmentManager(), "update_status_dialog");
                break;
//            case R.id.send_direct_messages:
//                intent = new Intent(this, PostActivity.class);
//                text = "D " + mUser.getScreenName() + " ";
//                intent.putExtra("status", text);
//                intent.putExtra("selection", text.length());
//                startActivity(intent);
//                break;
//            case R.id.add_to_list:
//                intent = new Intent(this, RegisterUserListActivity.class);
//                intent.putExtra("userId", mUser.getId());
//                startActivity(intent);
//                break;
            case R.id.open_web:
                final String[] items = getResources().getStringArray(R.array.dialog_menu_open_web);
                new AlertDialog.Builder(ProfileActivity.this)
                        .setItems(items, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int item) {
                                Intent intent;
                                switch (item) {
                                    case REQUEST_TWITTER:
                                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/"
                                                + mUser.getScreenName()));
                                        break;
                                    case REQUEST_FAVSTER:
                                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://ja.favstar.fm/users/"
                                                + mUser.getScreenName() + "/recent"));
                                        break;
                                    case REQUEST_ACLOG:
                                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://aclog.koba789.com/"
                                                + mUser.getScreenName() + "/timeline"));
                                        break;
                                    case REQUEST_TWILOG:
                                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://twilog.org/"
                                                + mUser.getScreenName()));
                                        break;
                                    default:
                                        return;
                                }
                                startActivity(intent);
                            }
                        })
                        .show();
                break;
            case R.id.report_spam:
                new AlertDialog.Builder(ProfileActivity.this)
                        .setMessage(R.string.confirm_report_spam)
                        .setPositiveButton(
                                R.string.button_report_spam,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        MessageUtil.showProgressDialog(ProfileActivity.this, getString(R.string.progress_process));
                                        ReportSpamTask task = new ReportSpamTask() {
                                            @Override
                                            protected void onPostExecute(Boolean success) {
                                                MessageUtil.dismissProgressDialog();
                                                if (success) {
                                                    MessageUtil.showToast(R.string.toast_report_spam_success);
                                                    restart();
                                                } else {
                                                    MessageUtil.showToast(R.string.toast_report_spam_failure);
                                                }

                                            }
                                        };
                                        task.execute(mUser.getId());
                                    }
                                }
                        )
                        .setNegativeButton(
                                R.string.button_cancel,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                }
                        )
                        .show();

                break;
            case R.id.create_block:
                new AlertDialog.Builder(ProfileActivity.this)
                        .setMessage(R.string.confirm_create_block)
                        .setPositiveButton(
                                R.string.button_create_block,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        MessageUtil.showProgressDialog(ProfileActivity.this, getString(R.string.progress_process));
                                        CreateBlockTask task = new CreateBlockTask() {
                                            @Override
                                            protected void onPostExecute(Boolean success) {
                                                MessageUtil.dismissProgressDialog();
                                                if (success) {
                                                    MessageUtil.showToast(R.string.toast_create_block_success);
                                                    restart();
                                                } else {
                                                    MessageUtil.showToast(R.string.toast_create_block_failure);
                                                }

                                            }
                                        };
                                        task.execute(mUser.getId());
                                    }
                                }
                        )
                        .setNegativeButton(
                                R.string.button_cancel,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                }
                        )
                        .show();

                break;
        }
        return true;
    }


    @Override
    public Loader<ProfileResponse> onCreateLoader(int id, Bundle args) {
        String screenName = args.getString(ProfileActivity.EXTRA_SCREEN_NAME);
        if (screenName != null) {
            return new UserLoader(this, screenName);
        } else {
            return new UserLoader(this, args.getLong(ProfileActivity.EXTRA_USER_ID));
        }
    }

    @Override
    public void onLoadFinished(Loader<ProfileResponse> loader, ProfileResponse response) {
        if (response == null) {
            MessageUtil.showToast(R.string.toast_load_data_failure);
            return;
        }
        response.visit(new ProfileResponse.Visitor() {
            @Override
            public void onSuccess(Profile profile) {
                mUser = profile.getUser();
                if (mUser == null) {
                    MessageUtil.showToast(R.string.toast_load_data_failure);
                    return;
                }

                setTitle(mUser.getName());
                mMainContainer.setVisibility(View.VISIBLE);
                mProgressBarContainer.setVisibility(View.GONE);
                mListView.setHeaderDividersEnabled(true);

                Relationship relationship = profile.getRelationship();
                mNameLabel.setText(mUser.getName());
                mFollowFlg = relationship.isSourceFollowingTarget();
                mBlocking = relationship.isSourceBlockingTarget();

                mNameLabel.setText(mUser.getName());
                mScreenNameLabel.setText("@" + mUser.getScreenName());

                if (mUser.isProtected()) {
                    mLockLabel.setVisibility(View.VISIBLE);
                }
                if (relationship.isSourceFollowedByTarget()) {
                    mFollowedByLabel.setText(R.string.label_followed_by_target);
                }

                if (mUser.getId() == AccessTokenManager.getUserId()) {
                    mFollowButton.setText(R.string.button_edit_profile);
                } else if (mFollowFlg) {
                    mFollowButton.setText(R.string.button_following);
                } else if (mBlocking) {
                    mFollowButton.setText(R.string.button_blocking);
                } else {
                    mFollowButton.setText(R.string.button_follow);
                }

                if (!TextUtils.isEmpty(mUser.getDescription())) {
                    String descriptionString = mUser.getDescription();

                    if (mUser.getDescriptionURLEntities() != null) {
                        URLEntity[] urls = mUser.getDescriptionURLEntities();
                        for (URLEntity descriptionUrl : urls) {
                            Pattern p = Pattern.compile(descriptionUrl.getURL());
                            Matcher m = p.matcher(descriptionString);
                            descriptionString = m.replaceAll(descriptionUrl.getExpandedURL());
                        }
                    }
                    mDescription.setText(descriptionString);
                } else {
                    mDescription.setVisibility(View.GONE);
                }

                if (!TextUtils.isEmpty(mUser.getLocation())) {
                    mLocation.setText(mUser.getLocation());
                } else {
                    mLocation.setVisibility(View.GONE);
                }

                if (!TextUtils.isEmpty(mUser.getURL())) {
                    if (mUser.getURLEntity() != null) {
                        mUrl.setText(mUser.getURLEntity().getExpandedURL());
                    } else {
                        mUrl.setText(mUser.getURL());
                    }
                } else {
                    mUrl.setVisibility(View.GONE);
                }

                final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(getString(R.string.format_user_created_at), Locale.ENGLISH);
                mStart.setText(simpleDateFormat.format(mUser.getCreatedAt()));

                mFavouritesCount.setText(String.format("%1$,3d", mUser.getFavouritesCount()));
                mStatusesCount.setText(String.format("%1$,3d", mUser.getStatusesCount()));
                mFriendsCount.setText(String.format("%1$,3d", mUser.getFriendsCount()));
                mFollowersCount.setText(String.format("%1$,3d", mUser.getFollowersCount()));

                String bannerUrl = mUser.getProfileBannerMobileRetinaURL();
                if (bannerUrl != null) {
                    ImageUtil.displayImage(bannerUrl, mBanner);
                }
                String iconUrl = mUser.getBiggerProfileImageURL();
                ImageUtil.displayRoundedImage(iconUrl, mInconImage);

                ResponseList<Status> statuses = profile.getStatuses();
                for (Status status : statuses) {
                    mAdapter.add(Row.newStatus(status));
                }

                LayoutInflater inflater = LayoutInflater.from(ProfileActivity.this);
                View footer = inflater.inflate(R.layout.guruguru, mListView, false);
                mListView.addFooterView(footer, null, false);
            }

            @Override
            public void onFailure(String errorMessage) {
                MessageUtil.showToast(errorMessage);
                finish();
            }
        });

    }

    @Override
    public void onLoaderReset(Loader<ProfileResponse> loader) {

    }

    public void restart() {
        Intent intent = new Intent(this, ProfileActivity.class);
        intent.putExtra(ProfileActivity.EXTRA_USER_ID, mUser.getId());
        startActivity(intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Fragment から startActivityForResult() で呼んだ Intent を
        // FragmentActivity の onActivityResult() で処理する場合は、下位16ビットだけで比較
        if (resultCode == RESULT_OK && (requestCode & 0xffff) == REQUEST_EDIT_PROFILE) {
            restart();
        }
    }

    @OnClick(R.id.follow_botton)
    void followButtonClick() {
        if (mUser.getId() == AccessTokenManager.getUserId()) {
            Intent intent = new Intent(this, EditProfileActivity.class);
            intent.putExtra(EditProfileActivity.EXTRA_USER, mUser);
            startActivityForResult(intent, ProfileActivity.REQUEST_EDIT_PROFILE);
        } else if (mFollowFlg) {
            new AlertDialog.Builder(this)
                    .setMessage(R.string.confirm_unfollow)
                    .setPositiveButton(
                            R.string.button_unfollow,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    MessageUtil.showProgressDialog(ProfileActivity.this, getString(R.string.progress_process));
                                    DestroyFriendshipTask task = new DestroyFriendshipTask() {
                                        @Override
                                        protected void onPostExecute(Boolean success) {
                                            MessageUtil.dismissProgressDialog();
                                            if (success) {
                                                MessageUtil.showToast(R.string.toast_destroy_friendship_success);
                                                mFollowButton.setText(R.string.button_follow);
                                                mFollowFlg = false;
                                            } else {
                                                MessageUtil.showToast(R.string.toast_destroy_friendship_failure);
                                            }
                                        }
                                    };
                                    task.execute(mUser.getId());
                                }
                            }
                    )
                    .setNegativeButton(
                            R.string.button_cancel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }
                    )
                    .show();
        } else if (mBlocking) {
            new AlertDialog.Builder(this)
                    .setMessage(R.string.confirm_destroy_block)
                    .setPositiveButton(
                            R.string.button_destroy_block,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    MessageUtil.showProgressDialog(ProfileActivity.this, getString(R.string.progress_process));
                                    DestroyBlockTask task = new DestroyBlockTask() {
                                        @Override
                                        protected void onPostExecute(Boolean success) {
                                            MessageUtil.dismissProgressDialog();
                                            if (success) {
                                                MessageUtil.showToast(R.string.toast_destroy_block_success);
                                                mFollowButton.setText(R.string.button_follow);
                                                mBlocking = false;
                                            } else {
                                                MessageUtil.showToast(R.string.toast_destroy_block_failure);
                                            }
                                        }
                                    };
                                    task.execute(mUser.getId());
                                }
                            }
                    )
                    .setNegativeButton(
                            R.string.button_cancel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }
                    )
                    .show();
        } else {
            new AlertDialog.Builder(this)
                    .setMessage(R.string.confirm_follow)
                    .setPositiveButton(
                            R.string.button_follow,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    MessageUtil.showProgressDialog(ProfileActivity.this, getString(R.string.progress_process));
                                    FollowTask task = new FollowTask() {
                                        @Override
                                        protected void onPostExecute(Boolean success) {
                                            MessageUtil.dismissProgressDialog();
                                            if (success) {
                                                MessageUtil.showToast(R.string.toast_follow_success);
                                                mFollowButton.setText(R.string.button_unfollow);
                                                mFollowFlg = true;
                                            } else {
                                                MessageUtil.showToast(R.string.toast_follow_failure);
                                            }
                                        }
                                    };
                                    task.execute(mUser.getId());
                                }
                            }
                    )
                    .setNegativeButton(
                            R.string.button_cancel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }
                    )
                    .show();
        }
    }

    @OnClick(R.id.icon_image)
    void enlargeIcon() {
        Intent intent = new Intent(this, ScaleImageActivity.class);
        intent.putExtra(ScaleImageActivity.EXTRA_URL, mUser.getOriginalProfileImageURL());
        startActivity(intent);
    }

    // TODO:
    @OnClick(R.id.list_button)
    void showListInfo() {

    }
}