package jp.tsur.twishi.ui.settings;


import android.app.ActionBar;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import jp.tsur.twishi.AccessTokenManager;
import jp.tsur.twishi.R;
import jp.tsur.twishi.TabManager;
import jp.tsur.twishi.adapter.SubscribeUserListAdapter;
import jp.tsur.twishi.event.AlertDialogEvent;
import jp.tsur.twishi.event.DestroyUserListEvent;
import jp.tsur.twishi.model.UserListCache;
import jp.tsur.twishi.model.UserListWithRegistered;
import jp.tsur.twishi.task.UserListsLoader;
import twitter4j.ResponseList;
import twitter4j.UserList;

public class ChooseUserListsActivity extends FragmentActivity
        implements LoaderManager.LoaderCallbacks<ResponseList<UserList>> {

    @InjectView(R.id.list_view)
    ListView mListView;
    @InjectView(R.id.progress_bar_container)
    ViewGroup mProgressBarContainer;

    private SubscribeUserListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_user_lists);
        ButterKnife.inject(this);

        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        mListView.setDivider(getResources().getDrawable(R.drawable.list_divider));

        mAdapter = new SubscribeUserListAdapter(this, R.layout.list_item_checkbox_icon);
        mListView.setAdapter(mAdapter);

        Bundle args = new Bundle();
        args.putLong("user_id", AccessTokenManager.getUserId());
        getSupportLoaderManager().initLoader(0, args, this).forceLoad();
    }

    @OnClick(R.id.button_save)
    void save() {
        HashMap<Long, Boolean> checkMap = new HashMap<>();
        ArrayList<UserList> checkList = new ArrayList<>();
        int count = mAdapter.getCount();
        for (int i = 0; i < count; i++) {
            UserListWithRegistered userListWithRegistered = mAdapter.getItem(i);
            UserList userList = userListWithRegistered.getUserList();
            if (userListWithRegistered.isRegistered()) {
                checkMap.put(userList.getId(), true);
                checkList.add(userList);
            }
        }
        HashMap<Long, Boolean> tabMap = new HashMap<>();
        ArrayList<TabManager.Tab> tabs = new ArrayList<>();
        for (TabManager.Tab tab : TabManager.loadTabs()) {
            if (tabMap.get(tab.id) != null) {
                continue;
            }
            if (tab.id < 0 || checkMap.get(tab.id) != null) {
                tabs.add(tab);
                tabMap.put(tab.id, true);
            }
        }
        for (UserList userList : checkList) {
            if (tabMap.get(userList.getId()) != null) {
                continue;
            }
            TabManager.Tab tab = new TabManager.Tab(userList.getId());
            if (userList.getUser().getId() == AccessTokenManager.getUserId()) {
                tab.name = userList.getName();
            } else {
                tab.name = userList.getFullName();
            }
            tabs.add(tab);
            tabMap.put(tab.id, true);
        }
        TabManager.saveTabs(tabs);
        setResult(RESULT_OK);
        finish();
    }

    @OnClick(R.id.button_cancel)
    void cancel() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @SuppressWarnings("UnusedDeclaration")
    public void onEventMainThread(AlertDialogEvent event) {
        event.getDialogFragment().show(getSupportFragmentManager(), "dialog");
    }

    @SuppressWarnings("UnusedDeclaration")
    public void onEventMainThread(DestroyUserListEvent event) {
        UserListWithRegistered userListWithRegistered = mAdapter.findByUserListId(event.getUserListId());
        if (userListWithRegistered != null) {
            mAdapter.remove(userListWithRegistered);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(RESULT_CANCELED);
                finish();
                break;
        }
        return true;
    }

    @Override
    public Loader<ResponseList<UserList>> onCreateLoader(int id, Bundle args) {
        return new UserListsLoader(this, args.getLong("user_id"));
    }

    @Override
    public void onLoadFinished(Loader<ResponseList<UserList>> loader, ResponseList<UserList> userLists) {
        if (userLists != null) {
            mProgressBarContainer.setVisibility(View.GONE);
            mListView.setVisibility(View.VISIBLE);

            for (UserList userList : userLists) {
                UserListWithRegistered userListWithRegistered = new UserListWithRegistered();
                userListWithRegistered.setRegistered(TabManager.hasTabId(userList.getId()));
                userListWithRegistered.setUserList(userList);
                mAdapter.add(userListWithRegistered);
            }
        }
        UserListCache.setUserLists(userLists);
    }

    @Override
    public void onLoaderReset(Loader<ResponseList<UserList>> loader) {
    }
}
