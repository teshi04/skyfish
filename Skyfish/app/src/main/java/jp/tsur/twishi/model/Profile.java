package jp.tsur.twishi.model;

import twitter4j.Relationship;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.User;


public class Profile {

    private User user;
    private Relationship relationship;
    private ResponseList<Status> statuses;

    public Profile(User user, Relationship relationship, ResponseList<Status> statuses) {
        this.user = user;
        this.relationship = relationship;
        this.statuses = statuses;
    }

    public ResponseList<Status> getStatuses() {
        return statuses;
    }

    public User getUser() {
        return user;
    }

    public Relationship getRelationship() {
        return relationship;
    }
}