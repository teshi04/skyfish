package jp.tsur.twishi.ui.settings;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import jp.tsur.twishi.R;
import jp.tsur.twishi.TabManager;
import jp.tsur.twishi.widget.FontelloTextView;

public class TabSettingsActivity extends ActionBarActivity {

    private static final int REQUEST_CHOOSE_USER_LIST = 100;
    private static final int HIGH_LIGHT_COLOR = Color.parseColor("#9933b5e5");
    private static final int DEFAULT_COLOR = Color.TRANSPARENT;

    @InjectView(R.id.list_view)
    ListView mListView;

    private TabAdapter mAdapter;
    private TabManager.Tab mDragTab;
    private boolean mSortable = false;
    private int mToPosition;
    private boolean mRemoveMode = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_settings);
        ButterKnife.inject(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        mListView.setDivider(getResources().getDrawable(R.drawable.list_divider));

        mAdapter = new TabAdapter(this, R.layout.list_item_tab_setting, TabManager.loadTabs());

        mListView.setAdapter(mAdapter);
        mListView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (!mSortable) {
                    return false;
                }
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        break;
                    }
                    case MotionEvent.ACTION_MOVE: {
                        int position = mListView.pointToPosition((int) motionEvent.getX(), (int) motionEvent.getY());
                        if (position < 0) {
                            break;
                        }
                        if (position != mToPosition) {
                            mToPosition = position;
                            mAdapter.remove(mDragTab);
                            mAdapter.insert(mDragTab, mToPosition);
                        }
                        return true;
                    }
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL:
                    case MotionEvent.ACTION_OUTSIDE: {
                        mAdapter.setCurrentTab(null);
                        mSortable = false;
                        return true;
                    }
                }
                return false;
            }
        });
    }

    @OnCheckedChanged(R.id.mode_switch)
    void switchMode(boolean checked) {
        mRemoveMode = checked;
        mAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.button_save)
    void save() {
        setResult(RESULT_CANCELED);
        TabManager.saveTabs(mAdapter.getTabs());
        setResult(RESULT_OK);
        finish();
    }

    @OnClick(R.id.button_cancel)
    void cancel() {
        setResult(RESULT_CANCELED);
        finish();
    }

    public void startDrag(TabManager.Tab tab) {
        mDragTab = tab;
        mToPosition = 0;
        mSortable = true;
        mAdapter.setCurrentTab(mDragTab);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.tab_setting, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem homeMenuItem = menu.findItem(R.id.add_home_tab);
        if (homeMenuItem != null) {
            homeMenuItem.setVisible(!mAdapter.hasTabId(TabManager.TIMELINE_TAB_ID));
        }
        MenuItem interactionsMenuItem = menu.findItem(R.id.add_interactions_tab);
        if (interactionsMenuItem != null) {
            interactionsMenuItem.setVisible(!mAdapter.hasTabId(TabManager.INTERACTIONS_TAB_ID));
        }
        MenuItem directMessagesMenuItem = menu.findItem(R.id.add_direct_messages_tab);
        if (directMessagesMenuItem != null) {
            directMessagesMenuItem.setVisible(!mAdapter.hasTabId(TabManager.DIRECT_MESSAGES_TAB_ID));
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.add_home_tab:
                mAdapter.insert(new TabManager.Tab(TabManager.TIMELINE_TAB_ID), 0);
                break;
            case R.id.add_interactions_tab:
                mAdapter.insert(new TabManager.Tab(TabManager.INTERACTIONS_TAB_ID), 0);
                break;
            case R.id.add_direct_messages_tab:
                mAdapter.insert(new TabManager.Tab(TabManager.DIRECT_MESSAGES_TAB_ID), 0);
                break;
            case R.id.select_to_list:
                TabManager.saveTabs(mAdapter.getTabs());
                Intent intent = new Intent(this, ChooseUserListsActivity.class);
                setResult(RESULT_OK);
                startActivityForResult(intent, REQUEST_CHOOSE_USER_LIST);
                break;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CHOOSE_USER_LIST:
                if (resultCode == RESULT_OK) {
                    mAdapter.clear();
                    mAdapter.addAll(TabManager.loadTabs());
                    mAdapter.notifyDataSetChanged();
                    mListView.invalidateViews();
                    setResult(RESULT_OK);
                }
                break;
        }
    }

    public class TabAdapter extends ArrayAdapter<TabManager.Tab> {

        class ViewHolder {
            @InjectView(R.id.handle)
            FontelloTextView mHandle;
            @InjectView(R.id.tab_icon)
            FontelloTextView mTabIcon;
            @InjectView(R.id.name)
            TextView mName;

            ViewHolder(View view) {
                ButterKnife.inject(this, view);
            }
        }

        private ArrayList<TabManager.Tab> mTabs = new ArrayList<>();
        private LayoutInflater mInflater;
        private int mLayout;
        private TabManager.Tab mCurrentTab;

        public TabAdapter(Context context, int resource, List<TabManager.Tab> tabList) {
            super(context, resource, tabList);
            mInflater = LayoutInflater.from(context);
            mLayout = resource;
            mTabs = (ArrayList<TabManager.Tab>) tabList;
        }

        public void setCurrentTab(TabManager.Tab tab) {
            mCurrentTab = tab;
            notifyDataSetChanged();
        }

        public ArrayList<TabManager.Tab> getTabs() {
            return mTabs;
        }

        public boolean hasTabId(long tabId) {
            for (TabManager.Tab tab : mTabs) {
                if (tab.id == tabId) {
                    return true;
                }
            }
            return false;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;

            // ビューを受け取る
            View view = convertView;
            if (view == null) {
                // 受け取ったビューがnullなら新しくビューを生成
                view = mInflater.inflate(this.mLayout, null);
                if (view == null) {
                    return null;
                }
                viewHolder = new ViewHolder(view);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }

            final TabManager.Tab tab = getItem(position);

            viewHolder.mTabIcon.setText(tab.getIcon());
            viewHolder.mName.setText(tab.getName());

            if (mRemoveMode) {
                viewHolder.mHandle.setText(R.string.fontello_trash);
                viewHolder.mHandle.setOnTouchListener(null);
                viewHolder.mHandle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mAdapter.remove(tab);
                    }
                });
            } else {
                viewHolder.mHandle.setText(R.string.fontello_menu);
                viewHolder.mHandle.setOnClickListener(null);
                viewHolder.mHandle.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                            startDrag(tab);
                            return true;
                        }
                        return false;
                    }
                });
            }

            if (mCurrentTab != null && mCurrentTab == tab) {
                view.setBackgroundColor(HIGH_LIGHT_COLOR);
            } else {
                view.setBackgroundColor(DEFAULT_COLOR);
            }

            return view;
        }
    }
}
