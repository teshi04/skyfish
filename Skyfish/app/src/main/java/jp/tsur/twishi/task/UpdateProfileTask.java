package jp.tsur.twishi.task;

import android.os.AsyncTask;

import jp.tsur.twishi.TwitterManager;
import twitter4j.User;

/**
 * プロフィールを更新
 */
public class UpdateProfileTask extends AsyncTask<String, Void, User> {
    @Override
    protected User doInBackground(String... params) {
        String name = params[0];
        String url = params[1];
        String location = params[2];
        String description = params[3];

        try {
            return TwitterManager.getTwitter().updateProfile(name, url, location, description);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}