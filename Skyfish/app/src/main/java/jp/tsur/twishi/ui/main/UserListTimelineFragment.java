package jp.tsur.twishi.ui.main;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.View;

import jp.tsur.twishi.model.Row;
import jp.tsur.twishi.task.UserListStatusesLoader;
import twitter4j.ResponseList;
import twitter4j.Status;


public class UserListTimelineFragment extends BaseMainTlFragment
        implements LoaderManager.LoaderCallbacks<ResponseList<Status>> {

    public static final String EXTRA_USER_LIST_ID = "user_list_id";
    private long mUserListId = 0L;

    @Override
    public long getTabId() {
        return mUserListId;
    }

    @Override
    protected boolean skip(Row row) {
//        return mMembers.get(row.getStatus().getUser().getId()) == null;
        // TODO: そもそも呼ばれないようにする
        return true;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mUserListId = getArguments().getLong(EXTRA_USER_LIST_ID);
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    protected void initLoader() {
        Bundle args = new Bundle();
        args.putLong(EXTRA_USER_LIST_ID, mUserListId);
        getLoaderManager().initLoader((int) getTabId(), args, this).forceLoad();
    }

    @Override
    protected void restartLoader() {
        Bundle args = new Bundle();
        if (!mReload) {
            args.putLong("max_id", mMaxId);
            args.putLong(EXTRA_USER_LIST_ID, mUserListId);
        }
        getLoaderManager().restartLoader(0, args, this).forceLoad();
    }

    @Override
    public Loader<ResponseList<Status>> onCreateLoader(int id, Bundle args) {
        return new UserListStatusesLoader(getActivity(), args.getLong("max_id", 0L), args.getLong(EXTRA_USER_LIST_ID));
    }

    @Override
    public void onLoadFinished(Loader<ResponseList<Status>> loader, ResponseList<Status> statuses) {
        setListShown(true);
        if (statuses == null || statuses.size() == 0) {
            mReload = false;
            mPullToRefreshLayout.setRefreshComplete();
            return;
        }
        if (mReload) {
            clear();
            for (twitter4j.Status status : statuses) {
                mAdapter.add(Row.newStatus(status));
            }
            mReload = false;
            mPullToRefreshLayout.setRefreshComplete();
        } else {
            for (twitter4j.Status status : statuses) {
                mAdapter.extensionAdd(Row.newStatus(status));
            }
            mAutoLoader = true;
        }

        mMaxId = statuses.get(statuses.size() - 1).getId();
    }

    @Override
    public void onLoaderReset(Loader<ResponseList<Status>> loader) {

    }
}
