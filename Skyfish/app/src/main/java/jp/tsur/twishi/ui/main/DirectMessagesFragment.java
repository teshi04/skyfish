package jp.tsur.twishi.ui.main;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import java.util.Collections;
import java.util.Comparator;

import jp.tsur.twishi.TabManager;
import jp.tsur.twishi.model.MyDirectMessage;
import jp.tsur.twishi.model.Row;
import jp.tsur.twishi.task.DirectMessageTimelineLoader;
import twitter4j.DirectMessage;
import twitter4j.ResponseList;

public class DirectMessagesFragment extends BaseMainTlFragment
        implements LoaderManager.LoaderCallbacks<MyDirectMessage> {

    protected long mDirectMessagesMaxId; // 読み込んだ最新の受信メッセージID
    protected long mSentDirectMessagesMaxId; // 読み込んだ最新の送信メッセージID

    public long getTabId() {
        return TabManager.DIRECT_MESSAGES_TAB_ID;
    }

    @Override
    protected void initLoader() {
        Bundle args = new Bundle();
        getLoaderManager().restartLoader(0, args, this).forceLoad();
    }

    @Override
    protected void restartLoader() {
        Bundle args = new Bundle();
        if (!mReload) {
            args.putLong("dm_max_id", mDirectMessagesMaxId);
            args.putLong("sent_max_id", mSentDirectMessagesMaxId);
        }
        getLoaderManager().restartLoader((int) getTabId(), args, this).forceLoad();
    }

    /**
     * このタブに表示するツイートの定義
     *
     * @param row ストリーミングAPIから受け取った情報（ツイートやDM）
     * @return trueは表示しない、falseは表示する
     */
    @Override
    protected boolean skip(Row row) {
        return !row.isDirectMessage();
    }

    @Override
    public void clear() {
        super.clear();
        mDirectMessagesMaxId = 0L;
        mSentDirectMessagesMaxId = 0L;
    }

    @Override
    public Loader<MyDirectMessage> onCreateLoader(int id, Bundle args) {
        return new DirectMessageTimelineLoader(getActivity(), args.getLong("dm_max_id"), args.getLong("sent_max_id"));
    }


    @Override
    public void onLoadFinished(Loader<MyDirectMessage> loader, MyDirectMessage myDirectMessages) {
        setListShown(true);

        if (myDirectMessages == null) {
            mReload = false;
            mPullToRefreshLayout.setRefreshComplete();
            return;
        }
        ResponseList<DirectMessage> directMessages = myDirectMessages.getDirectMessage();
        ResponseList<DirectMessage> sentDirectMessages = myDirectMessages.getSentDirectMessage();

        directMessages.addAll(sentDirectMessages);

        // 日付でソート
        Collections.sort(directMessages, new Comparator<DirectMessage>() {

            @Override
            public int compare(DirectMessage arg0, DirectMessage arg1) {
                return arg1.getCreatedAt().compareTo(arg0.getCreatedAt());
            }
        });

        if (mReload) {
            clear();
            for (DirectMessage status : directMessages) {
                mAdapter.add(Row.newDirectMessage(status));
            }
            mReload = false;
            mPullToRefreshLayout.setRefreshComplete();
        } else {
            for (DirectMessage status : directMessages) {
                mAdapter.extensionAdd(Row.newDirectMessage(status));
            }
            mAutoLoader = true;
        }

        if (directMessages.size() > 0) {
            mDirectMessagesMaxId = directMessages.get(directMessages.size() - 1).getId();
        }
        if (sentDirectMessages.size() > 0) {
            mSentDirectMessagesMaxId = sentDirectMessages.get(sentDirectMessages.size() - 1).getId();
        }
    }

    @Override
    public void onLoaderReset(Loader<MyDirectMessage> loader) {

    }

//    /**
//     * DM削除通知
//     * @param event DMのID
//     */
//    public void onEventMainThread(StreamingDestroyMessageEvent event) {
//        mAdapter.removeDirectMessage(event.getStatusId());
//    }
}
