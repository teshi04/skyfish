package jp.tsur.twishi.task;

import android.os.AsyncTask;

import jp.tsur.twishi.TwitterManager;
import jp.tsur.twishi.model.PostStock;
import twitter4j.HashtagEntity;
import twitter4j.StatusUpdate;
import twitter4j.TwitterException;

/**
 * ツイートする
 */
public class UpdateStatusTask extends AsyncTask<StatusUpdate, Void, TwitterException> {

    @Override
    protected TwitterException doInBackground(StatusUpdate... params) {
        StatusUpdate statusUpdate = params[0];
        try {
            twitter4j.Status status = TwitterManager.getTwitter().updateStatus(statusUpdate);

            PostStock postStock = new PostStock();
            for (HashtagEntity hashtagEntity : status.getHashtagEntities()) {
                postStock.addHashtag("#".concat(hashtagEntity.getText()));
            }
        } catch (TwitterException e) {
            return e;
        }
        return null;
    }
}