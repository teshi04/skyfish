package jp.tsur.twishi.model;

import twitter4j.DirectMessage;
import twitter4j.ResponseList;


public class MyDirectMessage {

    ResponseList<DirectMessage> mDirectMessage;
    ResponseList<DirectMessage> mSentDirectMessage;

    public MyDirectMessage(ResponseList<DirectMessage> directMessage, ResponseList<DirectMessage> sentDirectMessage) {
        mDirectMessage = directMessage;
        mSentDirectMessage = sentDirectMessage;
    }

    public ResponseList<DirectMessage> getDirectMessage() {
        return mDirectMessage;
    }

    public ResponseList<DirectMessage> getSentDirectMessage() {
        return mSentDirectMessage;
    }

}